package ru.sberbank.bigdata.cloud.p4d.fns_exp

import java.io.File

import org.apache.log4j.{Level, Logger}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{DateHelper, FileHelper}


object SelfLogger
{
  private[this] lazy val logFile: File = new File(s"work_log_${DateHelper.getCurrentTime("yyyy-MM-dd_HH-mm-ss")}.log")
  private[this] val saveLogToFile: Boolean = Runner.conf.printLogFile() != 0

  def printLog(textMessage: String, typeMessage: String = "INFO", newLine: Boolean = true): Unit =
  {
    val message: String = s"\n${DateHelper.getCurrentTime("yyyy-MM-dd HH:mm:ss,SSS")} $typeMessage $textMessage"
    if (newLine) println(message)
    else  print(message)
    if (saveLogToFile) FileHelper.writeTextToFile(message, logFile)
  }

  def setLoggerLevel(level: String): Unit =
    level match {
      case "ERROR" => Logger.getRootLogger.setLevel(Level.ERROR)
    }


}

