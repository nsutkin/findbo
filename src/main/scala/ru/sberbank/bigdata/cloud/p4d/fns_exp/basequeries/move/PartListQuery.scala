package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.move

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{BaseSqlQuery, SqlQuery}

class PartListQuery(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends BaseSqlQuery {

  override def getQueryText: String =
    s"""
       |SELECT DISTINCT explode(part_list) as part_list
       |FROM $sourceName.kpd_report
       |WHERE ctl_validfrom = '${queryData("max_ctl_validfrom")}'
       |AND entity = '$tableName'
     """.stripMargin
}
