package ru.sberbank.bigdata.cloud.p4d.fns_exp.scenario

import org.apache.spark.sql.DataFrame
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.AreaHelper
import ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild.ViewsManager

trait Scenario {

  def run(): Boolean
  def createOrReplaceTempViewFromArea (tblName:String,areaName:String, cch: Boolean = true):(String,String)={
    val fromStgQuery: String = s"SELECT * FROM $tblName"
    val df: DataFrame = QueryExecutor.queryToDataFrame(fromStgQuery)
    if (cch) df.cache()
    createOrReplaceTempViewFromDf(tblName,df, false)
  }

  def createOrReplaceTempViewFromDf (tblName:String, df:DataFrame,cch: Boolean = true):(String,String)={
    val viewName: String = tblName.concat("_view")
    if (cch) df.cache()
    ViewsManager.createOrReplaceTempViewFromDF(viewName, df)
    (tblName->viewName)
  }
}
