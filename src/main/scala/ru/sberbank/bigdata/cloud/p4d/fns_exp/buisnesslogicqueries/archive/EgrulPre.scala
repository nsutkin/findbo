package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.archive

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.SqlQuery

class EgrulPre(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends SqlQuery{

  override def getQueryText: String = {
    raw"""select
       |trim(regexp_replace(inn, ${queryData("regStr1")} ,' ')) as inn
       |,trim(regexp_replace(kpp, ${queryData("regStr1")} ,' ')) as kpp
       |,trim(regexp_replace(ogrn, ${queryData("regStr1")} ,' ')) as ogrn
       |,trim(regexp_replace(dataogrn, ${queryData("regStr1")} ,' ')) as dataogrn
       |,svnaimul
       |,svadrelpochty
       |,svadresul
       |,svokved
       |,svstatus
       |,svprekrul
       |,trim(regexp_replace(datavyp, ${queryData("regStr1")} ,' ')) as datavyp
       |,trim(regexp_replace(file_name, ${queryData("regStr1")} ,' ')) as file_name
       |,trim(regexp_replace(ctl_validfrom, ${queryData("regStr1")} ,' ')) as ctl_validfrom
       |,trim(regexp_replace(kodopf, ${queryData("regStr1")} ,' ')) as kodopf
       |,trim(regexp_replace(polnnaimopf, ${queryData("regStr1")} ,' ')) as polnnaimopf
       |,trim(regexp_replace(spropf, ${queryData("regStr1")} ,' ')) as spropf
       |,svuchredit
       |from $sourceName.egrul""".stripMargin
  }
}

