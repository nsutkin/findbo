package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.move

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.BaseSqlQuery

class LastForceLoadFlagQuery(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends BaseSqlQuery {
  override def getQueryText: String =
    s"""
       |SELECT isForceLoad from $sourceName.kpd_report
       |WHERE entity = '$tableName'
       |AND ctl_validfrom = '${queryData("max_ctl_validfrom")}'
     """.stripMargin
}
