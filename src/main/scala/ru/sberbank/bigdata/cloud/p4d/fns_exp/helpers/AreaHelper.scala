package ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers

import org.apache.hadoop.fs.{FileSystem, Path}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.DDLsFilesNotFoundException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Conf, Runner, SelfLogger}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor.queryToDataFrame

import scala.io.Source
import scala.util.matching.Regex

object AreaHelper {
  private[this] val conf: Conf = Runner.conf
  private[this] val productName: String = conf.productName()
  private[this] var areasTables: Map[String, List[String]] = Map.empty

  lazy val jarPath: String = new java.io.File(this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI).getCanonicalFile.getParent
  lazy val tgtDirHqlPathStr: String = s"$jarPath/../hive"
  lazy val localFileSystem: FileSystem = FileHelper.getLocalFileSystem
  lazy val factDirHqlPathStr: String = if (localFileSystem.exists(new Path(tgtDirHqlPathStr))
    && localFileSystem.isDirectory(new Path(tgtDirHqlPathStr))) tgtDirHqlPathStr else jarPath
  lazy val factDirHqlPath: Path = new Path(factDirHqlPathStr)

  def recreateArea(area: String): Unit = {
    SelfLogger.printLog(s"Try to recreate $area external tables...")
    val hqlFileNameTail: String = if (area.toLowerCase().equals("pa")) s"$productName.hql" else s"${productName}_$area.hql"
    val hqlFiles = localFileSystem.listStatus(factDirHqlPath).filter(_.isFile).filter(x=>x.getPath.getName.endsWith(hqlFileNameTail))
    val fileNames: String = hqlFiles.map(_.getPath.getName).mkString(",")


    SelfLogger.printLog(s"Recreate tables from files : $fileNames")

    if (hqlFiles.isEmpty) {
      SelfLogger.printLog(s"There doesn't exists hql-files in $factDirHqlPathStr","ERROR")
      throw new DDLsFilesNotFoundException
    }

    val listQueryPa = (for(x <- hqlFiles) yield
      scala.io.Source.fromFile(x.getPath.toUri.getRawPath).mkString.trim.split(";")).flatten.toList

    listQueryPa.foreach {
      query => queryToDataFrame(query, printLog = false)
    }
    SelfLogger.printLog(s"SUCCESS recreate $area external tables!")
  }

  def getAreaTables(area: String, hqlDirPath: String = factDirHqlPathStr): List[String] = {
    if (areasTables.getOrElse(area, List()).isEmpty) {
      areasTables = areasTables.updated(area, areaTablesNamesFromDDLs(area, hqlDirPath))
      areasTables(area)
    }
    else areasTables(area)
  }

  private def areaTablesNamesFromDDLs(area: String, hqlDirPath: String = factDirHqlPathStr): List[String] = {
    val hqlFileNameTail: String = if (area.toLowerCase.equals("pa")) s"$productName.hql"
                                  else s"${productName}_${area.toLowerCase}.hql"
    val hqlScriptPath: Path = FileHelper.getPathsInDirForNameTail(hqlDirPath, hqlFileNameTail).head
    val createLines: List[String] = {
      Source.fromFile(hqlScriptPath.toString, "UTF-8").getLines().toList.filter(x => x.toUpperCase.contains("CREATE"))
    }
    val tablesNames: List[String] =
      for (createLine <- createLines) yield "\\.[A-Za-z0-9_]{1,}".r.findFirstIn(createLine).mkString
      tablesNames
  }


  def getAreaDir(area: String): String = area.toLowerCase() match {
    case "pa" =>  StringHelper.removeLastSlash(conf.paDir())
    case "stg" => StringHelper.removeLastSlash(conf.stgDir())
    case "bkp" => StringHelper.removeLastSlash(conf.bkpDir())
    case _ =>
      SelfLogger.printLog(s"No directory for area $area", "ERROR")
      throw new IllegalArgumentException
  }

  def getAreaDb(area: String): String = area.toLowerCase() match {
    case "pa" =>  conf.paDb()
    case "stg" => conf.stgDb()
    case "bkp" => conf.bkpDb()
    case "ini" => conf.iniDb()
    case _ =>
      SelfLogger.printLog(s"No scheme for area $area", "ERROR")
      throw new IllegalArgumentException
  }

}
