package ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild

import java.util.Calendar

import org.apache.spark.sql.DataFrame
import ru.sberbank.bigdata.cloud.p4d.fns_exp._
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.{AreaWarehouse, Table}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.TablesBuildException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, DateHelper, FileHelper, StringHelper, TableHelper}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.parquetwork.Repart

// Класс представляет из себя полный цикл создания одной(!) сущности(таблицы)
trait EntityBuilder {
  val startTime: Long
  val tableName: String
  val threadTeg = s"THREAD $tableName INFO"

  def execute(): Int = {
    try {
      build()
      0
    }
    catch {
      case e: Throwable =>
        SelfLogger.printLog(e.getMessage, s"THREAD $tableName ERROR")
        e.printStackTrace()
        1
        throw new TablesBuildException
    }
  }

  def build(): DataFrame

  // Метод выполняет сохранение дата фрейма таблицы и проверяет наличие файлов после попытки записи.
  // Если файлов нет - таблица пуста.
  protected def trySaveTableAndCheckNewFiles(tableDF: DataFrame, mode: String = "append"): Boolean = {
//    FileHelper.writeDataFrameToParquet(tableDF, "", tableName, mode)
//    true
    val table: Table = AreaWarehouse.getArea("stg").getTable(tableName)

    // Определяем имя партиции для записи в hdfs и маску для репартиционирования
    val tablePartitionName: String = if (table.havePartitions) table.partitions.head else ""
    val tablePartitionMask: String = if (tablePartitionName.nonEmpty) tablePartitionName.concat("=*") else ""

    //tableDF.printSchema()

    // Запись в hdfs
    FileHelper.writeDataFrameToParquet(tableDF, tablePartitionName, tableName, mode)
    table.cleanGarbage

    // Проверка наличия изменений, запись статуса изменений, если изменений нет выходим из метода
    val tableHaveFiles: Boolean = table.haveFiles
    FileHelper.storeDataToFile(s"change_$tableName", s"${if (tableHaveFiles) "1" else "0"}")
    if (!tableHaveFiles) {
      SelfLogger.printLog(s"Table $tableName not have new data files in ${table.directory}.")
      return false
    }


    val maxSrcDt = getValidFrom(table.fields.mkString(","), tableDF)
    val maxBusDt = Option(tableDF.selectExpr("max(validfrom)").collect().head.getString(0)).getOrElse("")
    if (maxSrcDt!="") {
      FileHelper.storeDataToFile(s"src_max_dt_dm_$tableName", maxSrcDt)
    }
    if (maxBusDt!="") {
      FileHelper.storeDataToFile(s"bus_max_dt_dm_$tableName", maxBusDt)
    }

    // Приведение файлов к приблизительно правильному размеру (размер блока hdfs)
    Repart.repartTable(table.directory, tablePartitionMask)

    // получаем информацию для kpd_report
    // part_list
    val part_list: Array[String] = getPartListFromTableDF(tableDF, tablePartitionName)
    if (tablePartitionName.nonEmpty && part_list.isEmpty) {
      SelfLogger.printLog(s"Table $tableName not have new partition.")
      return false
    }
    val src_ctl_validfrom: String = getValidFrom(table.fields.mkString(","), tableDF)
    RecoveryManager.insertStep(tableName, TableHelper.getTableArea(tableName),getEndTime/1000, part_list = part_list, src_ctl_validfrom = src_ctl_validfrom)
    true
  }

  protected def getEndTime: Long = Calendar.getInstance().getTimeInMillis - startTime

  protected def getPartListFromTableDF(tableDF: DataFrame, partitionName: String): Array[String] = {
    if (partitionName.isEmpty) Array()
    else tableDF.select(partitionName).distinct().collect().map(_.getString(0))
  }

  protected def getValidFrom(fields: String, tableDF: DataFrame): String = {
    if (fields.contains("src_ctl_validfrom"))
      Option(tableDF.selectExpr("max(src_ctl_validfrom)").collect().head.getString(0)).getOrElse("")
    else if (fields.contains("ctl_validfrom"))
      Option(tableDF.selectExpr("max(ctl_validfrom)").collect().head.getString(0)).getOrElse("")
    else if (fields.contains("load_dt"))
      Option(tableDF.selectExpr("max(load_dt)").collect().head.getString(0)).getOrElse("")
    else ""
  }

  protected def getChangeMessage(haveNewData: Boolean): String =
    s"Table ${if (haveNewData) "have change." else "does not have change."}"

  protected def getReadyDataFromStg: DataFrame =
    QueryExecutor.queryToDataFrame(s"SELECT * FROM ${AreaHelper.getAreaDb("stg")}.$tableName").cache()

}
