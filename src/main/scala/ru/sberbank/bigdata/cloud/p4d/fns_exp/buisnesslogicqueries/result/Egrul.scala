package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.result

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.SqlQuery

class Egrul(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends SqlQuery{

  override def getQueryText: String = {
    raw"""select
       |s5.inn as inn
       |,s5.kpp as kpp
       |,trim(regexp_replace(s5.svnaimul.naimulpoln, ${queryData("regStr1")} ,' ')) as fullname
       |,trim(regexp_replace(s5.svnaimul.naimulsokr, ${queryData("regStr1")} ,' ')) as name
       |,trim(regexp_replace(s5.svadrelpochty.e_mail, ${queryData("regStr1")} ,' ')) as email
       |,if(s5.svadresul.adresrf is null,null,named_struct(
       |'indeks'
       |,nvl(trim(regexp_replace(s5.svadresul.adresrf.indeks, ${queryData("regStr1")} ,' ')),'')
       |,'kodregion'
       |,nvl(trim(regexp_replace(s5.svadresul.adresrf.kodregion, ${queryData("regStr1")} ,' ')),'')
       |,'kodadrkladr'
       |,nvl(trim(regexp_replace(s5.svadresul.adresrf.KodAdrKladr, ${queryData("regStr1")} ,' ')),'')
       |,'dom'
       |,nvl(trim(regexp_replace(s5.svadresul.adresrf.dom, ${queryData("regStr1")} ,' ')),'')
       |,'korpus'
       |,nvl(trim(regexp_replace(s5.svadresul.adresrf.korpus, ${queryData("regStr1")} ,' ')),'')
       |,'kvart'
       |,nvl(trim(regexp_replace(s5.svadresul.adresrf.kvart, ${queryData("regStr1")} ,' ')),'')
       |,'region'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadresul.adresrf.region.tipregion,s5.svadresul.adresrf.region.naimregion), ${queryData("regStr1")} ,' ')),'')
       |,'raion'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadresul.adresrf.raion.tipraion,s5.svadresul.adresrf.raion.naimraion), ${queryData("regStr1")} ,' ')),'')
       |,'gorod'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadresul.adresrf.gorod.tipgorod,s5.svadresul.adresrf.gorod.naimgorod), ${queryData("regStr1")} ,' ')),'')
       |,'naselpunkt'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadresul.adresrf.naselpunkt.tipnaselpunkt,s5.svadresul.adresrf.naselpunkt.naimnaselpunkt), ${queryData("regStr1")} ,' ')),'')
       |,'ulica'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadresul.adresrf.ulica.tipulica,s5.svadresul.adresrf.ulica.naimulica), ${queryData("regStr1")} ,' ')),'')
       |)) as address
       |,s5.ogrn as ogrn
       |,s5.dataogrn as ogrn_date
       |,s5.kodopf
       |,s5.polnnaimopf as naimopf
       |,s5.spropf
       |,if(s5.svuchredit is null,'0','1') as isfilial
       |,if(s5.svokved.svokvedosn is null,null,s4.okved) as okved
       |,if(s5.svstatus is null,null,array(named_struct(
       |'state_code'
       |,nvl(trim(regexp_replace(s5.svstatus[0].svstatus.kodstatusul, ${queryData("regStr1")} ,' ')),'')
       |,'state_name'
       |,nvl(trim(regexp_replace(s5.svstatus[0].svstatus.naimstatusul, ${queryData("regStr1")} ,' ')),'')
       |,'state_except_date'
       |,nvl(trim(regexp_replace(s5.svstatus[0].svreshisklul.dataresh, ${queryData("regStr1")} ,' ')),'')
       |,'state_except_num'
       |,nvl(trim(regexp_replace(s5.svstatus[0].svreshisklul.nomerresh, ${queryData("regStr1")} ,' ')),'')
       |,'state_except_pub_date'
       |,nvl(trim(regexp_replace(s5.svstatus[0].svreshisklul.datapublikacii, ${queryData("regStr1")} ,' ')),'')
       |,'state_except_mag_num'
       |,nvl(trim(regexp_replace(s5.svstatus[0].svreshisklul.nomerzhurnala, ${queryData("regStr1")} ,' ')),'')))) as state
       |,trim(regexp_replace(s5.svprekrul.dataprekrul, ${queryData("regStr1")} ,' ')) as date_of_termination
       |,trim(regexp_replace(s5.svprekrul.spprekrul.kodspprekrul, ${queryData("regStr1")} ,' ')) as code_of_termination
       |,trim(regexp_replace(s5.svprekrul.spprekrul.naimspprekrul, ${queryData("regStr1")} ,' ')) as name_of_termination
       |,s5.datavyp as validfrom
       |,cast('${queryData("ctl_validfrom")}'  as STRING) as load_date
       |,s5.file_name as filename
       |,cast('${queryData("ctl_loading")}' as STRING) as session_id
       |,s5.ctl_validfrom as src_ctl_validfrom
       |,cast('I' as STRING) as ctl_action
       |,cast('${queryData("ctl_validfrom")}'  as STRING) as ctl_validfrom
       |,cast('${queryData("ctl_loading")}' as BIGINT) as ctl_loading
       |,from_unixtime(unix_timestamp(s5.datavyp,'yyyy-MM-dd'), 'yyyy-MM') as validfrom_month
       |from ${queryData("egrul_pre")} s5
       |inner join
       |(select s3.ogrn, collect_set(s3.okved) as okved from
       |(select s.ogrn
       |,named_struct(
       |	'kodokved',nvl(trim(regexp_replace(s2.svokveddop.kodokved, ${queryData("regStr1")} ,' ')),'')
       |	,'naimokved',nvl(trim(regexp_replace(s2.svokveddop.naimokved, ${queryData("regStr1")} ,' ')),'')
       |	,'main','0') as okved
       |from ${queryData("egrul_pre")} s
       |lateral view outer explode(s.svokved.svokveddop) s2 as svokveddop
       |where s.svokved.svokveddop is not null
       |union all
       |select s.ogrn
       |,named_struct(
       |	'kodokved',nvl(trim(regexp_replace(s.svokved.svokvedosn.kodokved, ${queryData("regStr1")} ,' ')),'')
       |	,'naimokved',nvl(trim(regexp_replace(s.svokved.svokvedosn.naimokved, ${queryData("regStr1")} ,' ')),'')
       |	,'main','1') as okved
       |from ${queryData("egrul_pre")} s) s3
       |group by s3.ogrn)  s4 on s4.ogrn = s5.ogrn""".stripMargin

  }
}
