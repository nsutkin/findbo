package ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers

import java.io.{BufferedInputStream, BufferedReader, BufferedWriter, File, FileNotFoundException, FileOutputStream, IOException, InputStreamReader, OutputStreamWriter}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, FileSystem, Path}
import org.apache.hadoop.io._
import org.apache.spark.sql.{DataFrame, SaveMode}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Runner, SelfLogger}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.CheckException.check
import java.util.zip.{ZipEntry, ZipOutputStream}

import org.apache.spark.sql.functions.to_json
import org.apache.spark.sql.types.TimestampType

import scala.collection.immutable.ListMap

object FileHelper
{
  val hdfs: FileSystem = getFileSystem
  val localFileSystem: FileSystem = FileSystem.getLocal(new Configuration())
  val jarPath: String = new File(this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI).getCanonicalFile.getParent
  var outDirectoryPath = new Path(s"$jarPath/../../tmp")

  def directoryIsEmpty(str: String): Boolean = {
    SelfLogger.printLog(s"Check directory $str is empty")
    val path  = new Path(str)
    val status = getDirFilesStatuses(path)
    var isEmpty = false
    if (status.isEmpty) isEmpty = true
    SelfLogger.printLog(s"Directory $str ${if (isEmpty) "is" else "not"} empty", "WARN")
    isEmpty
  }

  private def checkExistOrRedirectOutDir(): Unit = {
    if (!localFileSystem.exists(outDirectoryPath) || !localFileSystem.isDirectory(outDirectoryPath))
    {outDirectoryPath = new Path(jarPath)}
  }

  def storeDataToFile(fileName: String, data: String): Unit = {
    checkExistOrRedirectOutDir()

    val srcMaxDateFile = new Path(outDirectoryPath, s"$fileName.txt")

    SelfLogger.printLog(s"Try to storing ${srcMaxDateFile.toString}")

    val maxDTFileStream = localFileSystem.create(srcMaxDateFile)
    maxDTFileStream.write(s"$data\n".getBytes("UTF-8"))
    maxDTFileStream.close()

    println(s"SUCCESS: ${srcMaxDateFile.toString}")
  }

  def cleanDirectory(dirPath: String, saveKey: String = ""): Unit = {
    SelfLogger.printLog(s"Try to clean directory $dirPath")
    val path: Path = new Path(dirPath)
    check(hdfs.exists(path), s"Directory $dirPath does not exist. Clear FAIL", new FileNotFoundException())

    var status: Array[FileStatus] = hdfs.listStatus(path)
    if(saveKey.nonEmpty) {
      SelfLogger.printLog("Method have save key")
//      SelfLogger.printLog(s"Save files list: ${status.filter(_.getPath.toString.contains(saveKey)).map(_.getPath.toString).mkString(",")}")
      status = status.filter(!_.getPath.getName.contains(saveKey))
      SelfLogger.printLog(s"Delete files list: ${status.map(_.getPath.toString).mkString(",")}")
    }

    if (status.nonEmpty) {
      for (file <- status) {
        SelfLogger.printLog(s"Delete file ${file.getPath.toString}")
        check(hdfs.delete(file.getPath, true), s"Delete file ${file.getPath.toString} FAIL", new IOException())
      }
      SelfLogger.printLog(s"Clean $dirPath SUCCESS")
    }
    else SelfLogger.printLog(s"Directory $dirPath is empty")
  }

  def getDirFilesStatuses: Path => List[FileStatus] = hdfs.listStatus(_).toList

  def getPathsInDirForNameTail(targetDir: Path, fileNameTail: String): List[Path] = {
    if (!localFileSystem.isDirectory(targetDir)) {
      SelfLogger.printLog(s"Target path ${targetDir.toString} is not directory", "ERROR")
      throw new IOException()
    }
    val files: Array[FileStatus] = localFileSystem.listStatus(targetDir).filter(_.isFile).filter(x=>x.getPath.getName.endsWith(fileNameTail))
    val filesPaths: Array[Path] = for (file <- files) yield new Path(targetDir, file.getPath.getName)
    if (filesPaths.isEmpty)
      SelfLogger.printLog(s"Did not find files in dir ${targetDir.toString} for file name tail $fileNameTail", "WARN")
    filesPaths.toList
  }

  def getPathsInDirForNameTail(targetDir: String, fileNameTail: String): List[Path] = getPathsInDirForNameTail(new Path(targetDir), fileNameTail)

  def writeDataFrameToParquet(df: DataFrame, partitionName: String, tableName: String,
                              mode: String = "append"): Unit = {
    val threadTeg = s"THREAD $tableName INFO"
    SelfLogger.printLog(s"partition: $partitionName")
    SelfLogger.printLog(s"Storing table $tableName data to hdfs...", threadTeg)
    if (partitionName.nonEmpty) writeDataFrameWithPartition(df, partitionName, tableName, mode)
    else writeDataFrameWithoutPartition(df, tableName, mode)
    SelfLogger.printLog(s"Storing table $tableName data SUCCESS", threadTeg)
  }

  private def writeDataFrameWithPartition(df: DataFrame, partitionName: String, tableName: String, mode: String): Unit = {
    df.write
      .partitionBy(partitionName)
      .mode(getSaveMode(mode))
      .parquet(s"${StringHelper.removeLastSlash(Runner.conf.stgDir())}/$tableName")
    SelfLogger.printLog(s"Repair table $tableName")
    TableHelper.repairTables("stg", tableName)
  }

  private def writeDataFrameWithoutPartition(df: DataFrame, tableName: String, mode: String): Unit = {
    df.write
      .mode(getSaveMode(mode))
      .parquet(s"${StringHelper.removeLastSlash(Runner.conf.stgDir())}/$tableName")
  }

  private def getSaveMode(mode: String): SaveMode = mode.toLowerCase match {
    case "append" => org.apache.spark.sql.SaveMode.Append
    case "overwrite" => org.apache.spark.sql.SaveMode.Overwrite
    case _ => throw new IllegalArgumentException
  }

  def getFileSystem: FileSystem = FileSystem.get(new Configuration())

  def getLocalFileSystem: FileSystem = localFileSystem

  def writeTextToFile(message: String, targetFile: File, isImportant: Boolean = false): Unit = {
    val writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(targetFile, true)))
    try {
      writer.write(message + "\n")
      writer.flush()
    }
    catch {
      case e: IOException =>
        SelfLogger.printLog("The program cannot write text to file!")
        if (isImportant) throw e
    }
    finally if (writer != null) writer.close()
  }

  // Получение MD5 файла
  def getMd5(pthStr:String,fs:FileSystem):String = {
    SelfLogger.printLog(s"Calculating MD5 of ${pthStr}")
    val pth = new Path(pthStr)
    val md5 = MD5Hash.digest(hdfs.open(pth)).toString
    println(s"SUCCESS: ${md5.toString}")
    md5
  }

  // Получение кол-ва записей
  def getCount (pthStr:String, fs:FileSystem, header:Boolean = true):Long = {
    SelfLogger.printLog(s"Count lines of ${pthStr}")
    val pth = new Path(pthStr)
    val br = new BufferedReader(new InputStreamReader(fs.open(pth)))
    var count = 0L
    var line = br.readLine()
    try
      while (line != null) {
        count += 1
        line=br.readLine()
      }
    finally
      br.close()

    if (count>0 && header)
      count -= 1
    println(s"SUCCESS: ${count}")
    count
  }

  // создание zip-архива
  def zip (out: Path, files: Iterable[Path],fs:FileSystem):Unit = {
    SelfLogger.printLog(s"Creating zip ${out}")
    val zip = new ZipOutputStream(fs.create(out))
    files.foreach { name =>
      zip.putNextEntry(new ZipEntry(name.getName))
      val in = new BufferedInputStream(fs.open(name))
      var b = in.read()
      while (b > -1) {
        zip.write(b)
        b = in.read()
      }
      in.close()
      zip.closeEntry()
      print(s"$name(${out.getName})${"."*10}SUCCESS")
    }
    zip.close()
  }

  // запись текстового файла
  def appendFile (out: Path, data:String, fs:FileSystem):Unit = {
    SelfLogger.printLog(s"Try to storing ${out.toString}")
    val in = if(fs.exists(out)) fs.append(out) else fs.create(out)
    in.write(s"$data\n".getBytes("UTF-8"))
    in.close()
    println(s"SUCCESS: ${out.toString}")
  }

  // сохранение Datframe в csv
  def saveDfAsCsv (df: DataFrame, numPart:Int, sep: String, header:Boolean, path: Path):Unit = {
    val clmns = df.schema.fields.map{x=>
       x.dataType.typeName match {
         case "struct" | "array" => to_json(df(x.name)).as(x.name)
         case _ => df(x.name)
       }
    }

    df.select(clmns:_*).repartition(numPart)
      .write
      .mode("overwrite")
      .option("delimiter",sep)
      .option("quote","\u0000")
      .format("csv")
      .option("header",header.toString)
      .save(path.toString)
  }

  def exportData (outPath: Path, entityName:String, df:DataFrame, fs:FileSystem, techFields:List[String],
                  sizePerRec:Double, tgtFileSize:Int, fileDt:String, ctlLoadingId:Long):Unit = {

    SelfLogger.printLog(s"Export data of $entityName...")
    SelfLogger.printLog(s"Drop tech-fields (${techFields.mkString(", ")})  from dataframe")
    val cleanDf = df.filter(df("ctl_loading") === ctlLoadingId)
      .drop(techFields:_*)

    // получение кол-ва записей
    SelfLogger.printLog("Calc count of whole table")
    val cnt:Long = cleanDf.count()
    println(s"SUCCESS: $cnt")

    if (cnt==0) {
      SelfLogger.printLog("No data to export")
      return
    }

    SelfLogger.printLog("Calc number of partition")
    val tableSize = cnt * sizePerRec / 1000
    val numPart:Int = if (tableSize<tgtFileSize) 1 else math.ceil(tableSize/tgtFileSize).toInt
    println(s"SUCESS: $numPart")

    SelfLogger.printLog("Saving csv-files")
    val exportPath = outPath
    val exportPatCsv = new Path(exportPath,entityName)
    FileHelper.saveDfAsCsv(cleanDf, numPart, "|", true, exportPatCsv)
    println("SUCCESS")

    SelfLogger.printLog("Ranaming initial csv-files")
    val statusList = FileHelper.getDirFilesStatuses(exportPatCsv)
    val srcCsvStatusList = statusList.filter(_.isFile).filter(x=>x.getPath.getName.endsWith(".csv")).sortBy(_.getPath.getName)

    val newNameFilePrefix = s"${entityName}_${fileDt}"
    val newNameFileList = (1 to srcCsvStatusList.size).map(x=>f"${newNameFilePrefix}_$x%02d")
    val newNameFileMap:Map[String,Path] = (newNameFileList zip srcCsvStatusList.map(_.getPath)).toMap
    val tgtNameFileMap:Map[String,Path]  = newNameFileMap.map{case(k,v) =>
      val  pth = new Path(v.getParent,k+".csv")
      FileHelper.hdfs.rename(v,pth)
      println(s"${v.getName} -> ${pth.getName}")
      (k -> pth)
    }

    val tgtNameFileMapSorted = ListMap(tgtNameFileMap.toSeq.sortBy(_._1):_*)

    SelfLogger.printLog("Calc and save md5, count of csv-files")

    tgtNameFileMapSorted.foreach{case(k,v)=>
      val pthStr =  v.toString
      println(pthStr)
      val md5 = FileHelper.getMd5(pthStr,FileHelper.hdfs)
      FileHelper.appendFile(new Path(exportPath,newNameFilePrefix+".md5"),s"$md5 ${v.getName}",FileHelper.hdfs)
      val cnt = FileHelper.getCount(pthStr,FileHelper.hdfs,true)
      FileHelper.appendFile(new Path(exportPath,newNameFilePrefix+".cnt"),s"${cnt.toString} ${v.getName}",FileHelper.hdfs)
    }

    SelfLogger.printLog("Zip csv-files")
    tgtNameFileMapSorted.foreach{case(k,v)=>
      FileHelper.zip(new Path(exportPath,k+".zip"),List(v),FileHelper.hdfs)
    }
  }

}