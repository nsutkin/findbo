package ru.sberbank.bigdata.cloud.p4d.fns_exp

import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.{AreaWarehouse, Table}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, FileHelper, TableHelper}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.parquetwork.{AreaTablesMove, Repart}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.resourcemonitor.ResourceMonitor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.scenario.{Scenario, ScenarioSelector}



object Runner {

  var conf: Conf = new Conf(Seq())
  val regStr: String = raw"'[^А-Яа-яA-Za-z0-9 ;/№\.\:\-]+'"

  def main(args: Array[String]): Unit = {

    // Парсинг аргументов
    createConf(args)

    // Установка уровня логирования
    SelfLogger.setLoggerLevel("ERROR")
    SelfLogger.printLog("VERSION 0.0.1")
    System.setProperty("HADOOP_USER_NAME", "hdfs")

    val spark: SparkSession = QueryExecutor.defaultSparkSessionGetOrCreate()

    val sparkContext: SparkContext = spark.sparkContext
    val resourceMonitor: ResourceMonitor = new ResourceMonitor(spark)
    val appIdValue: String = sparkContext.applicationId
    FileHelper.storeDataToFile("application_id", appIdValue)

     //Проверка работы в режиме recovery
   /* if (getRecoveryFlag) SelfLogger.printLog("Process start in recovery mode", "WARN")
    else FileHelper.cleanDirectory(conf.stgDir()) // на случай, если предыдущий поток не был успешен*/

    // пересоздаем таблицы всех областей
    AreaHelper.recreateArea("ini")
    AreaHelper.recreateArea("pa")
    AreaHelper.recreateArea("bkp")
    AreaHelper.recreateArea("stg")
   /* AreaWarehouse.getAreaTables("stg")
    AreaWarehouse.getAreaTables("pa")
    AreaWarehouse.getAreaTables("bkp")
    AreaWarehouse.getAreaTables("ini")*/

    // Получение сценария рассчета, "архивный" или "инкрементальный"
    val scenario: Scenario = ScenarioSelector.getScenario
    // Запуск выполнения сценария
    val haveChanges: Boolean = scenario.run()

    // Репартиционирование и подхват данных в таблице stg.kpd_report и получение из нее списка рассчитанных таблиц
    Repart.repartTable(AreaHelper.getAreaDir("stg").concat("/kpd_report"), "ctl_loading=*")
    TableHelper.repairTables("stg", "kpd_report")
    val changedTables: List[String] = TableHelper.getDoneTablesFromStg

    // Если какие либо таблицы были изменены, перемещаем данные PA в BKP и данные из STG в PA
    if (haveChanges && changedTables.nonEmpty) {
      SelfLogger.printLog("One or more tables has change. Try to get changed tables list.")
      SelfLogger.printLog(s"Changed tables list: ${changedTables.mkString(",")}")

      // Сохранение бэкапа
      new AreaTablesMove("pa", "bkp").moveTablesData()

      // Удаляем старые данные если форс лоад
      if (getForceLoadFlag) {
        val paTables: List[Table] = AreaWarehouse.getArea("pa").getTables
        paTables.filter(!_.tableName.equals("kpd_report")).foreach(table => FileHelper.cleanDirectory(table.directory))
        AreaHelper.recreateArea("pa")
      }

      // Перенос обновленных данных
      AreaHelper.recreateArea("stg")
      new AreaTablesMove("stg", "pa").moveTablesData()
    }
    else SelfLogger.printLog("DONE no changes.")
    if (getCleanupStg) FileHelper.cleanDirectory(conf.stgDir())


    SelfLogger.printLog(resourceMonitor.getResourceReport(appIdValue))

    spark.stop()

  }

  def createConf(args: Array[String]): Unit = {
    conf = new Conf(args)
    conf.printAllFields()
  }

  def getRecoveryFlag: Boolean = conf.etlRecoveryMode() != 0

  def getForceLoadFlag: Boolean = conf.etlForceLoad() != 0

  def getCleanupStg: Boolean = conf.etlCleanupStg() != 0


}
