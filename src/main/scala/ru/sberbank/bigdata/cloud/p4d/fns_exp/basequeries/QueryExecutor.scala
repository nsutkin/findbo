package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries

import org.apache.spark.sql.{AnalysisException, DataFrame, SparkSession}
import org.apache.spark.{SparkConf, SparkException}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Runner, SelfLogger}

import scala.util.{Failure, Success, Try}

object QueryExecutor
{
  def queryToDataFrame(query: SqlQuery): DataFrame = {
    queryToDataFrame(query.getQueryText)
  }

  def queryToDataFrame(queryText: String, sparkSession: SparkSession = defaultSparkSessionGetOrCreate(), printLog: Boolean = true): DataFrame =
  {
println(queryText + "++++++++++++++++++++++++++++")
    if (printLog) SelfLogger.printLog(s"Try to exequte query:\n $queryText")
    Try {
      sparkSession.sql(queryText)
    }
    match {
      case Success(x) => x
      case Failure(err: AnalysisException) =>
        SelfLogger.printLog(s"Query fails to analyze: \n $queryText", "ERROR")
        throw err
      case Failure(err) =>
        SelfLogger.printLog(s"Some Eception occured in query: \n$queryText\n", "ERROR")
        throw err
    }
  }

  def defaultSparkSessionGetOrCreate(): SparkSession =
  {
    try
    {
      SparkSession.builder().appName(Runner.conf.appName()).enableHiveSupport().getOrCreate()
     /*val sparkConf = new SparkConf
      sparkConf.setMaster("local[2]")*/
      //SparkSession.builder().appName("Test").enableHiveSupport().master("local[*]").getOrCreate()
    }
    catch
      {
        case _: SparkException => getOrCreateLocalSparkSession
      }
  }

  private[this] def getOrCreateLocalSparkSession: SparkSession =
  {
    val sparkConf = new SparkConf
    sparkConf.setMaster("local[2]")
    SparkSession.builder().appName("Test").enableHiveSupport().master("local[*]").getOrCreate()
  }

  def sessionStop(): Boolean =
  {
    defaultSparkSessionGetOrCreate().stop()
    true
  }

  /** Функция получения Dataframe таблицы
   * schemaName - наименование БД таблицы
   * tableName - наименование таблицы
   */
  /*def getDf(schemaName: String,tableName: String): DataFrame =
  {
    val session: SparkSession = defaultSparkSessionGetOrCreate()
    session.sql(s"""select t.* from $schemaName.$tableName t""".stripMargin)
  }*/
}
