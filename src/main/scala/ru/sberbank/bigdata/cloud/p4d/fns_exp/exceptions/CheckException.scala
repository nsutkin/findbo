package ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions

import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger

object CheckException {
  // Отличие от assert в собственном логировании и выборе пробрасываемого исключения
  def check(assert: Boolean, failMessage: String, throwable: Throwable): Unit = if (!assert) { // TODO вставить, где возможно
    SelfLogger.printLog(failMessage)
    throw throwable
  }

}
