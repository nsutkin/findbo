package ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild

import org.apache.spark.sql.DataFrame
import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor

class ViewFromQueryBuilder(val tableName: String, query: String) extends EntityBuilder {

  override val startTime: Long = 0L

  override def build(): DataFrame = {
    SelfLogger.printLog(s"Create temp view as query:\n $query")
    val viewDF: DataFrame = QueryExecutor.queryToDataFrame(query).cache()
    ViewsManager.createOrReplaceTempViewFromDF(tableName, viewDF)
    viewDF
  }
}
