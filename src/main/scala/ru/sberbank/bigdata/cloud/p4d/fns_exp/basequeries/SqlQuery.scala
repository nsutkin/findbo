package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries

trait SqlQuery {
  val tableName: String
  val sourceName: String
  val queryData: Map[String, String]

  def getQueryText: String
}
