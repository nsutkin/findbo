package ru.sberbank.bigdata.cloud.p4d.fns_exp

import org.rogach.scallop.{ScallopConf, ScallopOption}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.DateHelper

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val productName: ScallopOption[String] = opt[String](default = Some("fns_exp"))
  val appName: ScallopOption[String] = opt[String](default = Some("p4d-fns-exp"))
  val etlRecoveryMode: ScallopOption[Int] = opt[Int](default = Some(0))
  val etlForceLoad: ScallopOption[Int] = opt[Int](default = Some(0))
  val ctlLoading: ScallopOption[Long] = opt[Long](default = Some(999))
  val ctlValidfrom: ScallopOption[String] = opt[String](default = Some(DateHelper.getCurrentTime()))
  val iniDb: ScallopOption[String] = opt[String](default = Some("custom_cib_p4d_fns_exp_ini"))
  val stgDb: ScallopOption[String] = opt[String](default = Some("custom_cib_p4d_fns_exp_stg"))
  val stgDir: ScallopOption[String] = opt[String](default = Some("user/root/custom/cib/p4d/fns_exp/stg"))
  val bkpDb: ScallopOption[String] = opt[String](default = Some("custom_cib_p4d_fns_exp_bkp"))
  val bkpDir: ScallopOption[String] = opt[String](default = Some("/root/custom/cib/p4d/fns_exp/bkp"))
  val paDb: ScallopOption[String] = opt[String](default = Some("custom_cib_p4d_fns_exp"))
  val paDir: ScallopOption[String] = opt[String](default = Some("/root/custom/cib/p4d/fns_exp/pa"))
  val refPath: ScallopOption[String] = opt[String](default = Some("./tables.csv"))
  val etlCleanupStg: ScallopOption[Int] = opt[Int](default = Some(1))
  val concurrentCalculateTable: ScallopOption[Int] = opt[Int](default = Some(0), hidden = true)
  val printLogFile: ScallopOption[Int] = opt[Int](default = Some(0))
  verify()


  def printAllFields(): Unit =
  {
    SelfLogger.printLog("Conf fields after parsing:\n")
    println(this.filteredSummary(Set.empty)) // Выводит в консоль значения всех полей
  }
}