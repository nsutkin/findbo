package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.result

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.SqlQuery

class Egrip(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends SqlQuery{

  override def getQueryText: String = {
    raw"""select
       |s5.innfl as inn
       |,s5.ogrnip as ogrn
       |,s5.dataogrnip as ogrn_date
       |,trim(regexp_replace(s5.svfl.pol, ${queryData("regStr1")} ,' ')) as gender
       |,upper(concat_ws(' ',trim(regexp_replace(s5.svfl.fiorus.familiya, ${queryData("regStr1")} ,' '))
       |,trim(regexp_replace(s5.svfl.fiorus.imya, ${queryData("regStr1")} ,' '))
       |,trim(regexp_replace(s5.svfl.fiorus.otchestvo, ${queryData("regStr1")} ,' ')))) as fullname
       |,trim(regexp_replace(s5.svgrazhd.vidgrazhd, ${queryData("regStr1")} ,' ')) as citizenship_type
       |,case s5.svgrazhd.vidgrazhd
       | when '1' then 'Гражданин РФ'
       | when '2' then 'Иностранный гражданин'
       | when '3' then 'Без гражданства'
       |end as citizenship_name
       |,trim(regexp_replace(s5.svgrazhd.naimstran, ${queryData("regStr1")} ,' ')) as country
       |,if(s5.svadrmzh.adresrf is null,null,named_struct(
       |'indeks'
       |,''
       |,'kodregion'
       |,nvl(trim(regexp_replace(s5.svadrmzh.adresrf.kodregion, ${queryData("regStr1")} ,' ')),'')
       |,'kodadrkladr'
       |,''
       |,'dom'
       |,''
       |,'korpus'
       |,''
       |,'kvart'
       |,''
       |,'region'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadrmzh.adresrf.region.tipregion,s5.svadrmzh.adresrf.region.naimregion), ${queryData("regStr1")} ,' ')),'')
       |,'raion'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadrmzh.adresrf.raion.tipraion,s5.svadrmzh.adresrf.raion.naimraion), ${queryData("regStr1")} ,' ')),'')
       |,'gorod'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadrmzh.adresrf.gorod.tipgorod,s5.svadrmzh.adresrf.gorod.naimgorod), ${queryData("regStr1")} ,' ')),'')
       |,'naselpunkt'
       |,nvl(trim(regexp_replace(concat_ws(' ',s5.svadrmzh.adresrf.naselpunkt.tipnaselpunkt,s5.svadrmzh.adresrf.naselpunkt.naimnaselpunkt), ${queryData("regStr1")} ,' ')),'')
       |,'ulica'
       |,''
       |)) as address
       |,trim(regexp_replace(s5.svadrelpochty.e_mail, ${queryData("regStr1")} ,' ')) as email
       |,trim(regexp_replace(s5.svstatus.svstatus.kodstatus, ${queryData("regStr1")} ,' ')) as state_code
       |,trim(regexp_replace(s5.svstatus.svstatus.naimstatus, ${queryData("regStr1")} ,' ')) as state_name
       |,trim(regexp_replace(s5.svprekrash.svstatus.kodstatus, ${queryData("regStr1")} ,' ')) as term_state_code
       |,trim(regexp_replace(s5.svprekrash.svstatus.naimstatus, ${queryData("regStr1")} ,' ')) as term_state_name
       |,trim(regexp_replace(s5.svprekrash.svstatus.dataprekrash, ${queryData("regStr1")} ,' ')) as term_state_date
       |,if(s5.svokved.svokvedosn is null,null,s4.okved) as okved
       |,s5.datavyp as validfrom
       |,cast('${queryData("ctl_validfrom")}'  as STRING) as load_date
       |,s5.file_name as filename
       |,cast('${queryData("ctl_loading")}' as STRING) as session_id
       |,s5.ctl_validfrom as src_ctl_validfrom
       |,cast('I' as STRING) as ctl_action
       |,cast('${queryData("ctl_validfrom")}'  as STRING) as ctl_validfrom
       |,cast('${queryData("ctl_loading")}' as BIGINT) as ctl_loading
       |,from_unixtime(unix_timestamp(s5.datavyp,'yyyy-MM-dd'), 'yyyy-MM') as validfrom_month
       |from ${queryData("egrip_pre")}  s5
       |inner join
       |(select s3.ogrnip, collect_set(s3.okved) as okved from
       |(select s.ogrnip
       |,named_struct(
       |	'kodokved',nvl(trim(regexp_replace(s2.svokveddop.kodokved, ${queryData("regStr1")} ,' ')),'')
       |	,'naimokved',nvl(trim(regexp_replace(s2.svokveddop.naimokved, ${queryData("regStr1")} ,' ')),'')
       |	,'main','0') as okved
       |from ${queryData("egrip_pre")} s
       |lateral view outer explode(s.svokved.svokveddop) s2 as svokveddop
       |where s.svokved.svokveddop is not null
       |union all
       |select s.ogrnip
       |,named_struct(
       |	'kodokved',nvl(trim(regexp_replace(s.svokved.svokvedosn.kodokved, ${queryData("regStr1")} ,' ')),'')
       |	,'naimokved',nvl(trim(regexp_replace(s.svokved.svokvedosn.naimokved, ${queryData("regStr1")} ,' ')),'')
       |	,'main','1') as okved
       |from ${queryData("egrip_pre")} s) s3
       |group by s3.ogrnip) s4 on s4.ogrnip = s5.ogrnip""".stripMargin

  }
}
