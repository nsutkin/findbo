package ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild

import ru.sberbank.bigdata.cloud.p4d.fns_exp._
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.TablesBuildException

import scala.concurrent._
import ExecutionContext.Implicits.global


class BuildExecutor() {
  private[this] val concurrentEnable: Boolean = Runner.conf.concurrentCalculateTable() match {
    case 0 =>  true
    case 1 => false
    case _ => throw new IllegalArgumentException("Param concurrent-calculate-table should be 0 or 1")
  }

  def execute(entityBuilders: List[EntityBuilder], concurrentEnable: Boolean = concurrentEnable): Unit = {
    if (concurrentEnable) concurrentExecute(entityBuilders)
    else monoThreadExecute(entityBuilders)
  }

  def concurrentExecute(entityBuilders: List[EntityBuilder]): Unit = {
    // распаралеливание через scala Future, с проверкой кода выполнения
    // duration.Duration.Inf - неограниченное ожидание
    val executors: List[(String, Future[Int])] = entityBuilders.map(x => (x.tableName, Future{x.execute()}))
    val executeCode =
      for (executor <- executors) yield Await.result(executor._2, duration.Duration.Inf)
    SelfLogger.printLog(s"Execute codes: ${executeCode.mkString(",")}")
    SelfLogger.printLog("Build tables is done")
    if (!executeCode.contains(1)) {
      SelfLogger.printLog("All tables are successfully built")
    }
    else {
      SelfLogger.printLog("One or more tables built with error.\n \n SHUT DOWN", "ERROR")
      throw new TablesBuildException
    }
  }

  def monoThreadExecute(entityBuilders: List[EntityBuilder]): Unit = {
    val executeCode = for (builder <- entityBuilders) yield builder.execute()
    SelfLogger.printLog(s"Execute codes: ${executeCode.mkString(",")}")
    SelfLogger.printLog("Build tables is done")
    if (!executeCode.contains(1)) {
      SelfLogger.printLog("All tables are successfully built")
    }
    else {
      SelfLogger.printLog("One or more tables built with error.\n \n SHUT DOWN", "ERROR")
      throw new TablesBuildException
    }
  }

  private def executeCodeToStatus(code: Int): String = code match {
    case 0 => "SUCCESS"
    case 1 => "FAIL"
    case _ => throw new IllegalArgumentException
  }

}
