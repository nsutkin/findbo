package ru.sberbank.bigdata.cloud.p4d.fns_exp.resourcemonitor.fakessl

import java.security.cert.X509Certificate
import javax.net.ssl._

object TrustAll extends X509TrustManager {
  	val getAcceptedIssuers: Array[X509Certificate] = null
  	def checkClientTrusted(x509Certificates: Array[X509Certificate], s: String): Unit = {}
  	def checkServerTrusted(x509Certificates: Array[X509Certificate], s: String): Unit = {}
  	}

