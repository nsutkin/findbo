package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries

import ru.sberbank.bigdata.cloud.p4d.fns_exp.Runner

trait QueryManager {
  var queryData: Map[String, String] = Map.empty

  def getQueryForTable(tableName: String): SqlQuery

  // Обновление словаря данных для запросов
  def updateQueryData(key: String, value: String): Unit = queryData = queryData.updated(key, value)

  // Инициализация текущих ctl_validfrom и ctl_loading
  def initCtlData(): Unit = {
    this.updateQueryData("ctl_validfrom", Runner.conf.ctlValidfrom())
    this.updateQueryData("ctl_loading", s"${Runner.conf.ctlLoading()}")
  }

}
