package ru.sberbank.bigdata.cloud.p4d.fns_exp.entities

import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.entities.TableNotFoundInAreaException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.AreaHelper

class Area(val areaName: String) {

  private[this] var tables: List[Table] = List()


  def getTables: List[Table] = if (tables.isEmpty) {tables = initTables; tables} else tables
  def getTable(tableName: String): Table = {
    if (tables.isEmpty) tables = initTables
    if (tables.exists(_.tableName.equals(tableName))) tables.filter(_.tableName.equals(tableName)).head
    else throw new TableNotFoundInAreaException
  }

  private def initTables: List[Table] = for(tableName <- AreaHelper.getAreaTables(areaName)) yield new Table(areaName, tableName)

}
