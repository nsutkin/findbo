package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.move

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{BaseSqlQuery, SqlQuery}

class MaxCtlValidfromQuery(val tableName: String, val sourceName: String, val queryData: Map[String, String] = Map.empty) extends BaseSqlQuery {

  override def getQueryText: String =
    s"""
       |SELECT max(ctl_validfrom) FROM $sourceName.kpd_report
       |WHERE entity = '$tableName'
     """.stripMargin
}
