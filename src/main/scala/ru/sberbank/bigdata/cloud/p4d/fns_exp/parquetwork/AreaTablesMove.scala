package ru.sberbank.bigdata.cloud.p4d.fns_exp.parquetwork

import java.io.{FileNotFoundException, IOException}
import org.apache.hadoop.fs.{FileStatus, FileSystem, FileUtil, Path}
import org.apache.spark.sql.{DataFrame, Row}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.{AreaWarehouse, Table}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.CheckException.check
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryExecutor, SqlQuery}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.move.{LastForceLoadFlagQuery, MaxCtlValidfromQuery, PartListQuery}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.entities.parquetwork.{AreaTablesMoveParamException, TableAreaException, TablesCountException}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, FileHelper}

class AreaTablesMove(srcArea: String, tgtArea: String) {

  check((srcArea.equals("stg") && tgtArea.equals("aux"))
    ||(srcArea.equals("stg") && tgtArea.equals("pa"))
    ||(srcArea.equals("pa")  && tgtArea.equals("bkp")),
    s"Incorrect param pair for TableMove srcArea: $srcArea, tgtArea: $tgtArea. " +
      s"Correct pairs: 'stg' -> 'aux', 'stg' -> 'pa', 'pa' -> 'bkp'", new AreaTablesMoveParamException)

  val sourceSchema: String = AreaHelper.getAreaDb(srcArea)
  val targetSchema: String = AreaHelper.getAreaDb(tgtArea)
  val hdfs: FileSystem = FileHelper.hdfs

  def moveTablesData(): Unit = {
    SelfLogger.printLog(s"Start move area $srcArea tables data to area $tgtArea.")
    val tablesPairs: Map[Table, Table] = checkAndGenerateTablesPairs

    for (sourceTable <- tablesPairs.keys) {
      val targetTable: Table = tablesPairs.getOrElse(sourceTable, throw new TableAreaException)

      val isKpdReport: Boolean = sourceTable.tableName.contains("kpd_report")
      val tableHavePartitions: Boolean = sourceTable.havePartitions

      if (isKpdReport) moveKpdReport(sourceTable, targetTable)
      // Если в таблице нет партиционирования, то полный перенос, иначе  частичный
      else if (!tableHavePartitions) moveFullTableData(sourceTable, targetTable)
      else moveTableWithPartitions(sourceTable, targetTable)
    }
    AreaHelper.recreateArea(tgtArea)
    SelfLogger.printLog(s"Move area $srcArea tables data to area $tgtArea SUCCESS")
  }

  private def moveTableWithPartitions(sourceTable: Table, targetTable: Table): Unit = {
    // Находим дату последней загрузки таблицы
    val lastValidFrom: String = Option(getMaxCtlValidFrom(sourceTable)).getOrElse("")

    // Получаем force флаг последней загрузки, для понимания, нужен ли нам полный перенос таблицы или частичный
    val needFullMove: Boolean = lastValidFrom.isEmpty || lastLoadIsForce(sourceTable, lastValidFrom)

    // Если последняя загрузка была в force режиме, то полный перенос и выход из метода(return)
    if (needFullMove) moveFullTableData(sourceTable, targetTable)
    // Соответственно, если загрузка была без force режима, начинаем частичный перенос
    else movePartsTableData(sourceTable, targetTable, lastValidFrom)
    // Подхват данных
    sourceTable.repair()
  }

  private def moveFullTableData(sourceTable: Table, targetTable: Table): Unit = {
    SelfLogger.printLog(s"Full move for table ${sourceTable.tableName} start")
    val srcTableDir: Path = new Path(sourceTable.directory)

    if (!hdfs.exists(srcTableDir)) {
      SelfLogger.printLog(s"Source table ${sourceTable.tableName} directory ${srcTableDir.toString} does not exist")
      throw new FileNotFoundException()
    }

    if (FileHelper.directoryIsEmpty(srcTableDir.toString)) {
      SelfLogger.printLog(s"Source table ${sourceTable.tableName} directory ${srcTableDir.toString} " +
        s"is empty. Skip move", "WARN")
      return
    }

    val tgtTableDir: Path = new Path(targetTable.directory)
    if (hdfs.exists(tgtTableDir)) hdfs.delete(tgtTableDir, true)
    SelfLogger.printLog(s"Move ${srcTableDir.toString} ===> ${tgtTableDir.toString}")
    FileUtil.copy(hdfs, srcTableDir, hdfs, tgtTableDir, false, true, hdfs.getConf)
    SelfLogger.printLog(s"Full move for table ${sourceTable.tableName} complete")
  }

  private def moveKpdReport(srcKpdReport: Table, tgtKpdReport: Table): Unit = {
    SelfLogger.printLog("Start move kpd_report")
    val srcPath: Path = new Path(srcKpdReport.directory)

    val srcParts: Array[Path] = hdfs.listStatus(srcPath).map(_.getPath)
    SelfLogger.printLog(s"New kpd_report partitions: ${srcParts.map(_.toString).mkString(",")}")

    for (srcKpdPart <- srcParts) {
      val targetKpdPart: Path = new Path(tgtKpdReport.directory, srcKpdPart.getName)
      SelfLogger.printLog(s"Copy ${srcKpdPart.toString} ===> ${targetKpdPart.toString}")
      FileUtil.copy(hdfs, srcKpdPart, hdfs, targetKpdPart, false, true, hdfs.getConf)
    }

    SelfLogger.printLog("Move kpd_report SUCCESS")
  }

  private def movePartsTableData(srcTable: Table,  tgtTable: Table, maxValidFrom: String): Unit = {
    SelfLogger.printLog(s"Partitions backup for table ${srcTable.tableName} start")
    // Получаем пути к дирректориям таблиц
    val srcTablePath: Path = new Path(srcTable.directory)
    val tgtTablePath: Path = new Path(tgtTable.directory)
    // Получаем имя партиции
    val partitionName: String = srcTable.partitions.head
    // Получаем список значений обновляемых партиций
    val partitionsList: List[String] = getNewPartitions(srcTable, maxValidFrom)

    // Осуществляем перенос партиций
    for (partitionValue <- partitionsList) {
      val srcPath: Path = new Path(srcTablePath, s"$partitionName=$partitionValue")
      val tgtPath: Path = new Path(tgtTablePath, s"$partitionName=$partitionValue")
      SelfLogger.printLog(s"Overwrite ${srcPath.toString} ===> ${tgtPath.toString}")
      if (hdfs.exists(tgtPath)) check(hdfs.delete(tgtPath, true), s"Delete ${tgtPath.toString} FAIL", new IOException)
      FileUtil.copy(hdfs, srcPath, hdfs, tgtPath, false, true, hdfs.getConf)
    }
    SelfLogger.printLog(s"Partitions move for table ${srcTable.tableName} complete")
  }

  private def getMaxCtlValidFrom(table: Table): String = {
    val query: SqlQuery = new MaxCtlValidfromQuery(table.tableName, sourceSchema)
    val responce: Array[Row] = QueryExecutor.queryToDataFrame(query).collect()
    if (responce.nonEmpty && responce.head.getString(0) != null) responce.head.getString(0)
    else ""
  }

  private def getNewPartitions(table: Table, maxValidFrom: String): List[String] = {
    val partListQuery: SqlQuery = new PartListQuery(table.tableName, sourceSchema, Map("max_ctl_validfrom" -> maxValidFrom))
    val partListDF: DataFrame = QueryExecutor.queryToDataFrame(partListQuery)

    val newPartitionsNames: List[String] = partListDF.collect().toList.map{case org.apache.spark.sql.Row(x:String)=> s"$x"}
    if (newPartitionsNames.isEmpty)
      SelfLogger.printLog(s"Last load partitions list for table ${table.tableName} is empty", "WARN")
    newPartitionsNames
  }

  private def lastLoadIsForce(srcTable: Table, maxValidFrom: String): Boolean = {
    val isForceLoadDf: DataFrame =
      QueryExecutor.queryToDataFrame(new LastForceLoadFlagQuery(srcTable.tableName, sourceSchema, Map("max_ctl_validfrom" -> maxValidFrom)))

    // Извлекаем через option ,что бы избежать ошибки при пустом результате
    val forceLoadResponse: String = Option(isForceLoadDf.select("isForceLoad").collect().head.getString(0)).getOrElse("false")
    val lastLoadIsForce: Boolean = forceLoadResponse.equals("true")
    SelfLogger.printLog(s"Table ${srcTable.tableName} last forceLoad flag is $lastLoadIsForce")
    lastLoadIsForce
  }

  private def checkAndGenerateTablesPairs: Map[Table, Table] = {
    SelfLogger.printLog(s"Generate table pairs for move $srcArea ===> $tgtArea")
    val sourceTables: List[Table] = AreaWarehouse.getArea(srcArea).getTables
    val targetTables: List[Table] = AreaWarehouse.getArea(tgtArea).getTables
    SelfLogger.printLog(s"Source tables: ${sourceTables.map(_.tableName)}")
    SelfLogger.printLog(s"Target tables: ${targetTables.map(_.tableName)}")

    check(sourceTables.size >= targetTables.size,
      "The number of source tables, should be greater than or equal to the number of target tables",
      new TablesCountException)

    // Генерим пары таблиц (sourceTable, targetTable),
    // из списка target таблиц т.к. он всегда меньше или равен списку source
    val tablesPairs: List[(Table, Table)] =
      targetTables.map(table => (sourceTables.filter(_.tableName.equals(table.tableName)).head, table))
    Map(tablesPairs: _*)
  }

}
