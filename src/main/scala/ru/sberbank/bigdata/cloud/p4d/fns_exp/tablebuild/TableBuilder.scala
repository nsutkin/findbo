package ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild

import java.util.Calendar

import org.apache.spark.sql.DataFrame
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryExecutor, QueryManager, SqlQuery}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.AreaHelper
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{RecoveryManager, Runner, SelfLogger}

class TableBuilder(val tableName: String, queryManager: QueryManager, cch:Boolean = false) extends EntityBuilder {
  val startTime: Long = Calendar.getInstance().getTimeInMillis

  override def build(): DataFrame = {
    SelfLogger.printLog(s"Start table $tableName calculation.", s"$threadTeg")

    // Проверка рекавери и возврат даннх из stg
    if (Runner.getRecoveryFlag && RecoveryManager.tableIsComplete(tableName)) {
      SelfLogger.printLog(s"Table $tableName is complete in stg", s"$threadTeg RECOVERY")
      SelfLogger.printLog(s"Return table $tableName data frame from stg", s"$threadTeg RECOVERY")
      return getReadyDataFromStg
    }
    else if (Runner.getRecoveryFlag) SelfLogger.printLog(s"Table $tableName not complete in stg", s"$threadTeg RECOVERY")

    // Получаем запрос таблицы и создаем на ее основе data frame
    val tableQuery: SqlQuery = queryManager.getQueryForTable(tableName)
    val tableDF: DataFrame = QueryExecutor.queryToDataFrame(tableQuery)
    if (cch) {
      tableDF.cache()
      ViewsManager.createOrReplaceTempViewFromDF(tableName.concat("_view"), tableDF)
    }
    //tableDF.printSchema()
    // Сохранение data frame таблицы в hdfs
    val haveNewData: Boolean = trySaveTableAndCheckNewFiles(tableDF)

    // Возвращаем рассчитанный датафрейм
    SelfLogger.printLog(s"The calculation table $tableName is completed. " +
      s"${getChangeMessage(haveNewData)}", s"INFO $threadTeg")
    tableDF
  }

}
