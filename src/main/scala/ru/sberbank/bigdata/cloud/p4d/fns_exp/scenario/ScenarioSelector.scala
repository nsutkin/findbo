package ru.sberbank.bigdata.cloud.p4d.fns_exp.scenario

import ru.sberbank.bigdata.cloud.p4d.fns_exp.Runner.conf
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.ScenarioSelectException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, FileHelper, TableHelper}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Runner, SelfLogger}

object ScenarioSelector {

  lazy val isNotForceLoad: Boolean = !Runner.getForceLoadFlag
  lazy val isNotFirstLoad: Boolean = notFirstLoad
  lazy val isNotPaDirectoryEmpty: Boolean = !FileHelper.directoryIsEmpty(conf.paDir())

  def getScenario: Scenario = {
    //
   /* if (isNotForceLoad && isNotPaDirectoryEmpty && isNotFirstLoad) {
      new IncrementScenario
    }
    else {*/
      // Если запуск в  режиме forceload зачищаем область stg и пересоздаем таблицы
      val archiveScenarioCause: String = checkAndGetArchiveCause
      SelfLogger.printLog(s"Select archive scenario because: $archiveScenarioCause", "WARN")
      if (!FileHelper.directoryIsEmpty(conf.stgDir()) && !isNotForceLoad) FileHelper.cleanDirectory(conf.stgDir())
      AreaHelper.recreateArea("stg")
      AreaHelper.recreateArea("pa")
      new ArchiveScenario
    //}
  }

  private def notFirstLoad: Boolean = TableHelper.getNonEmptyTablesInArea("pa").nonEmpty

  private def checkAndGetArchiveCause: String = {
    if (!isNotForceLoad) "Force load ON"
    else if (!isNotPaDirectoryEmpty) "PA directory is empty"
    else if (!isNotFirstLoad) "First market calculate"
    else throw new ScenarioSelectException
  }

}
