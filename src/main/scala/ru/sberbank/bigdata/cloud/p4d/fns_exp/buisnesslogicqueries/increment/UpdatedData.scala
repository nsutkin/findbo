package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.increment

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.SqlQuery

class UpdatedData(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends SqlQuery{

  override def getQueryText: String =
    s"""
       |    select *
       |    from ${tableName}_view
     """.stripMargin

}
