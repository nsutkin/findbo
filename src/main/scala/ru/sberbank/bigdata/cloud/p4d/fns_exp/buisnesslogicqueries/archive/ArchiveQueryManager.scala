package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.archive

import ru.sberbank.bigdata.cloud.p4d.fns_exp.Runner
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryManager, SqlQuery}

class ArchiveQueryManager extends QueryManager{
  private[this] val iniDb: String = Runner.conf.iniDb()
  private[this] val stgDb: String = Runner.conf.stgDb()

  override def getQueryForTable(tableName: String): SqlQuery = tableName.toLowerCase match {
    case "egrip_pre" => new EgripPre("", iniDb, queryData)
    case "egrul_pre" => new EgrulPre("", iniDb, queryData)
    case _ => throw new IllegalArgumentException
  }

}
