package ru.sberbank.bigdata.cloud.p4d.fns_exp

import org.apache.spark.sql.SparkSession
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.Table
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{DateHelper, StringHelper, TableHelper}

object RecoveryManager {

  private[this] val sparkSession: SparkSession = QueryExecutor.defaultSparkSessionGetOrCreate()
  private lazy val completeTables: List[String] = TableHelper.getDoneTablesFromStg.filter(TableHelper.getNonEmptyTablesInArea("stg").contains(_))

  //процедура записи в таблицу kpd_report в stg
  def insertStep(entity: String
                 , area:String
                 , timeDuration: Long
                 , finishedDttm: String = DateHelper.getCurrentTime()
                 , isForceLoad: Boolean = Runner.getForceLoadFlag
                 , src_ctl_validfrom: String = ""
                 , ctl_validfrom: String = Runner.conf.ctlValidfrom()
                 , part_list: Array[String] = Array()
                 , ctl_loading: Long = Runner.conf.ctlLoading()
       ): Unit =
  {


    import sparkSession.implicits._
    Seq((entity
      , area
      , finishedDttm
      , timeDuration
      , ctl_validfrom
      , src_ctl_validfrom
      , isForceLoad.toString
      , part_list
      , ctl_loading
    ))
      .toDF("entity", "area", "entity_finished_dttm", "entity_duration_sec", "ctl_validfrom", "src_ctl_validfrom", "isForceLoad", "part_list", "ctl_loading")
      .write.partitionBy("ctl_loading").mode("Append").parquet(s"${StringHelper.removeLastSlash(Runner.conf.stgDir())}/kpd_report")
  }

  def tableIsComplete: String => Boolean = completeTables contains _

  def tableIsCompleteInStg: Table => Boolean = completeTables contains  _.tableName

  def filterCompletesTables: List[String] => List[String] = _.filter(!completeTables.contains(_))
  

}
