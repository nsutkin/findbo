package ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers

object StringHelper
{
  def removeLastSlash(s: String): String =
    if (s.endsWith("/"))
      s.substring(0, s.lastIndexOf('/'))
    else
      s

}
