package ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{DataFrame, SparkSession}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor.queryToDataFrame
import ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild.ViewsManager
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.Table
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.TableNotFoundInDDLException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Runner, SelfLogger}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.RecoveryManager

import scala.io.Source
import scala.util.matching.Regex

object TableHelper {
  // TODO разделить на CreateTableHelper и TableActionHelper
  private[this] val productName: String = Runner.conf.productName()

  lazy val jarPath: String = new java.io.File(this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI).getCanonicalFile.getParent
  lazy val tgtDirHqlPathStr: String = s"$jarPath/../hive"
  lazy val localFileSystem: FileSystem = FileHelper.getLocalFileSystem
  lazy val factDirHqlPathStr: String = if (localFileSystem.exists(new Path(tgtDirHqlPathStr))
    && localFileSystem.isDirectory(new Path(tgtDirHqlPathStr))) tgtDirHqlPathStr else jarPath
  lazy val factDirHqlPath: Path = new Path(factDirHqlPathStr)

  val insidePattern = "(?s)(?<=select).+(?=from)".r
  val parenthesesPattern = "\\(((?:[^)(]+|\\((?:[^)(]+|\\([^)(]*\\))*\\))*)\\)".r
  val newLinePattern = "(\r\n)|\r|\n|\t".r
  val quotesPattern = "'[^']+'".r
  val splitCommaPattern ="(?<=,)[^,]+(?=,)|[^,]+".r
  val lastWordPattern = "\\w+$".r
  val firstWordPattern = "^\\w+".r

  def findTableFields(area: String, tableName: String, hqlDirPath: String = factDirHqlPathStr,
                      withCtlFields: Boolean = false): List[String] = {
//     TODO сделать поддержку парсинга вьюх
    //if (area.equalsIgnoreCase("ini")) throw new UnsupportedOperationException
    val hqlFileNameTail: String = if (area.equalsIgnoreCase("pa")) s"$productName.hql"
                                  else s"${productName}_$area.hql"
    val hqlScriptPath: Path = FileHelper.getPathsInDirForNameTail(hqlDirPath, hqlFileNameTail).head
    val queries = for (line <- Source.fromFile(hqlScriptPath.toString, "UTF-8").mkString.trim.split(";")) yield line
    val targetQuery: String = queries.filter(x => x.contains(s".$tableName") && x.contains("CREATE")).head
    val targetQueryParts: Array[String] = targetQuery.split("PARTITIONED BY")

//    val headPattern = new Regex(s"$tableName [^0-9a-zA-Z]{1,}[A-Za-z0-9_]{1,} ")
//    val fieldsPattern = new Regex(",[A-Za-z0-9_]{1,}[^0-9a-zA-Z()]{1,}")
//    val head: String = (headPattern findFirstIn targetQueryParts.head).mkString("").split(" ").reverse.head
//    val fields: List[String] =
//      head.concat((fieldsPattern findAllIn targetQueryParts.head).mkString("")).split(",")
//      .toList
//      .filter(withCtlFields || !_.contains("ctl_")).map(_.trim)
//
//    val generalFields: String = fields.mkString(",")
//    val partitionFields: String = if (targetQueryParts.length > 1) getTablePartitionFields(targetQueryParts(1)) else ""

    val fields: List[String] =
      if (area=="ini") getFieldsFromSelect(targetQueryParts.head)
      else getFieldsFromFirstBlock(targetQueryParts.head).filter(withCtlFields || !_.contains("ctl_"))
    val generalFields: String = fields.mkString(",")
    val partitionFields: String = if (targetQueryParts.length > 1) getFieldsFromFirstBlock(targetQueryParts(1)).mkString(",") else ""
    List("table", "partitionFields")
  }

  def findTablePartitions(area: String, tableName: String, hqlDirPath: String = factDirHqlPathStr): List[String] = {
    val partitions: String = findTableFields(area, tableName, hqlDirPath)(1)
    if (partitions.isEmpty) List("")
    else partitions.split(",").toList
  }


  private def getTablePartitionFields(partitionQueryPart: String, castTypes: Boolean = false): String = {
    val partitionHeadPattern = new Regex(s"^ [(]{1,}[^0-9a-zA-Z]{1,}[A-Za-z0-9_]{1,} ")
    val partitionFieldsPattern = new Regex(",[A-Za-z0-9_]{1,} ")
    val partitionHead: String = (partitionHeadPattern findFirstIn partitionQueryPart).mkString("").trim.split(" ").reverse.head
    val partitionFields: String = (partitionFieldsPattern findAllIn partitionQueryPart).mkString(",")

    val result: String =  if (partitionFields.nonEmpty) partitionHead.substring(1).trim.concat(s",$partitionFields")
    else partitionHead.substring(1).trim
    result
  }

  def getFieldsFromFirstBlock (str:String): List[String]= {

    val parenthesesList = parenthesesPattern.findAllIn(str).matchData.toList
    val dataInside = if (parenthesesList.nonEmpty) parenthesesList.head.group(1) else return List()
    val dataWithoutNewline = newLinePattern.replaceAllIn(dataInside," ")
    val dataWithoutParentheses = parenthesesPattern.replaceAllIn(dataWithoutNewline," ")
    val dataWithoutQuotes = quotesPattern.replaceAllIn(dataWithoutParentheses,"")
    val fieldDefList = splitCommaPattern.findAllIn(dataWithoutQuotes).toList.map(_.trim).filter(_.nonEmpty)
    if (fieldDefList.isEmpty) return List()

    fieldDefList.map(x => firstWordPattern.findFirstIn(x).getOrElse(""))
  }

  def getFieldsFromSelect (str:String): List[String] = {

    val insideData = insidePattern.findFirstIn(str).getOrElse("")
    val dataWithoutNewline = newLinePattern.replaceAllIn(insideData," ")
    val dataWithoutParentheses = parenthesesPattern.replaceAllIn(dataWithoutNewline," ")
    val dataWithoutQuotes = quotesPattern.replaceAllIn(dataWithoutParentheses,"")
    val fieldDefList = splitCommaPattern.findAllIn(dataWithoutQuotes).toList.map(_.trim).filter(_.nonEmpty)

    fieldDefList.map(x => lastWordPattern.findFirstIn(x).getOrElse(""))
  }

  // Получение списка измененных таблиц
  def getDoneTablesFromStg: List[String] = {
    AreaHelper.recreateArea("stg")
    val changedTablesDf: DataFrame = queryToDataFrame(s"SELECT entity FROM ${AreaHelper.getAreaDb("stg")}.kpd_report",
      printLog = false).cache()
    val res: List[String] = for (row <- changedTablesDf.collect().toList) yield row.getString(0)
    SelfLogger.printLog(s"Ready tables is: ${if (res.isEmpty) "empty" else res.mkString(",")}")
    res
  }

  // Подхват данных таблицами
  def repairTables(area: String, tablesNames: String*): Unit = {
    for (tableName <- tablesNames)
      queryToDataFrame(s"MSCK REPAIR TABLE ${AreaHelper.getAreaDb(area)}.$tableName", printLog = false)
  }

  def getTableArea(tableName: String, hqlDirPath: String = factDirHqlPathStr): String = {
    if (AreaHelper.getAreaTables("pa", hqlDirPath).contains(tableName)) "pa"
    else if (AreaHelper.getAreaTables("aux", hqlDirPath).contains(tableName)) "aux"
    else if (AreaHelper.getAreaTables("stg", hqlDirPath).contains(tableName)) "stg"
    else {
      SelfLogger.printLog(s"Table name $tableName not found!", "ERROR")
      throw new TableNotFoundInDDLException
    }
  }


  def createDataFrameFromExistTable(area: String, tableName: String): DataFrame = {
    val tableQuery: String = s"SELECT * FROM ${AreaHelper.getAreaDb(area)}.$tableName"
    QueryExecutor.queryToDataFrame(tableQuery)
  }

  def createViewFromExistTable(area: String, tableName: String): Unit = {
    SelfLogger.printLog(s"Create view for ${AreaHelper.getAreaDb(area)}.$tableName")
    val viewDF: DataFrame = createDataFrameFromExistTable(area, tableName).cache()
    SelfLogger.printLog(s"View for ${AreaHelper.getAreaDb(area)}.$tableName count: ${viewDF.count()}") // TODO Delete after tests
    ViewsManager.createOrReplaceTempViewFromDF(tableName.concat("_view"), viewDF)
  }

  def createAndGetAreaTables(area: String): List[Table] = {
    val areaTablesNames: List[String] = AreaHelper.getAreaTables(area)
    for (tableName <- areaTablesNames) yield new Table(area, tableName)
  }

  // Метод проверяет наличие директорий таблиц, их файлов и наличие непустого результата запроса к ним(select *)
  def getNonEmptyTablesInArea(area: String): List[String] = {
    val allTablesNames: List[String] = AreaHelper.getAreaTables(area)
    allTablesNames.filter(x => tableFilesExists(area, x) && tableHaveResultForQuery(area, x))
  }

  // Проверка существования пути таблицы, что ее путь это директория и наличия файлов в директории таблицы
  // TODO перенестив класс Table
  private def tableFilesExists(area: String, tableName: String): Boolean = {
    val path = new Path(AreaHelper.getAreaDir(area) + "/" + tableName)
    SelfLogger.printLog(s"Check existing table $tableName directory in $area. Directory path: ${path.toString}")
    val fs = FileSystem.get(new Configuration())
    val isExist: Boolean = fs.exists(path) && fs.getFileStatus(path).isDirectory && !FileHelper.directoryIsEmpty(path.toString)
    // Запись в лог результатов проверки
    if (isExist) SelfLogger.printLog(s"Table $tableName directory is exists in $area")
    else SelfLogger.printLog(s"Table $tableName directory does not exists in $area")

    isExist
  }

  // Проверка наличия результата запроса к таблице (SELECT * FROM table LIMIT 100)
  // TODO move to class Table
   def tableHaveResultForQuery(area: String, tableName: String): Boolean = {
    SelfLogger.printLog(s"Check table $tableName have data in $area")

    val isNonEmptyTable: Boolean =
      QueryExecutor.queryToDataFrame(s"""select * from ${AreaHelper.getAreaDb(area)}.$tableName LIMIT 100""".stripMargin).head(1).nonEmpty
    // Запись в лог результатов проверки
    if (isNonEmptyTable) SelfLogger.printLog(s"Table $tableName having data in $area")
    else SelfLogger.printLog(s"Table $tableName does not have data in $area")

    isNonEmptyTable
  }

def saveIniTablesStatuses(maxMap:Map[String,String]):Unit ={
  maxMap.foreach{
    case(k,v) =>
      RecoveryManager.insertStep(k,"ini",0,part_list = Array(), src_ctl_validfrom = v)
  }

}

  /** Функция получение максимальных значений поля для списка таблиц
   * spark - Spark-сессия
   * tbls - список таблиц
   * clmn - наименование поля
   * schema - наименование БД содержащих таблицы
   */
  def getMaxDateTableMap (spark: SparkSession = QueryExecutor.defaultSparkSessionGetOrCreate(), tbls:List[String], clmn:String, schema:String): Map[String,String] = {
    SelfLogger.printLog(s"Creating dataframes to get maximum $clmn of tables: ${tbls.mkString(",")}")
    val srcStgDfMap=tbls.map(x=>(x,spark.sql(s"select $clmn  from ${schema}.$x"))).toMap
    SelfLogger.printLog(s"Getting maximum $clmn for each  table")
    val srcStgMaxMap = srcStgDfMap.map{case(k,v)=>(k,DateHelper.getMaxValuesFromDf(v,List(v.schema.fields(0).name))(clmn))}
    println("\n" + Tabulator.format(List(List("table",clmn))++srcStgMaxMap.map{case(k,v)=>Seq(k,v)}.toList))
    srcStgMaxMap
  }



}
