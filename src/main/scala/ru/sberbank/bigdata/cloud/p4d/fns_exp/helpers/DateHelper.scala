package ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers

import java.util.Calendar
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions.{from_unixtime, max, unix_timestamp,count,greatest}
import org.apache.spark.sql.types.TimestampType


object DateHelper {

  def getCurrentTime(format: String = "yyyy-MM-dd HH:mm:ss"): String = new java.text.SimpleDateFormat(format).
    format(Calendar.getInstance().getTime)


  def getMaxValuesFromDf(df: DataFrame, clmnName:List[String]):Map[String,String]= {
    val clmns = for(k<-clmnName) yield {
      df.schema(k).dataType match {
        case TimestampType => from_unixtime(unix_timestamp(max(k)),"yyyy-MM-dd HH:mm:ss")
        case _ =>  max(k)
      }
    }
    val maxDs = df.agg(clmns.head,clmns.tail:_*).head(1)
    (clmnName zip (0 to clmnName.length-1).map(x=>getRowStringByIndex(maxDs,x))).toMap
  }

  def getRowStringByIndex(arr:Array[Row],n:Int): String = {
    if (arr.isEmpty) return ""
    Option(arr(0).getString(n)).getOrElse(return "")
  }

}