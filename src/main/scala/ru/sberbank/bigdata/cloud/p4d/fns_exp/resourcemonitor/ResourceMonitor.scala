package ru.sberbank.bigdata.cloud.p4d.fns_exp.resourcemonitor

import java.net.SocketException
import javax.net.ssl.{HttpsURLConnection, SSLContext, SSLHandshakeException}

import org.apache.spark.sql.SparkSession
import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger
import ru.sberbank.bigdata.cloud.p4d.fns_exp.resourcemonitor.fakessl.{TrustAll, VerifiesAllHostNames}


class ResourceMonitor(val sparkSession: SparkSession)
{
  def getResourceReport(appId: String): String =
  {
    val yarnUrl: String = s"http://${getYarnUrl(appId)}/ws/v1/cluster/apps/$appId"
    SelfLogger.printLog(s"Try to get resource info from $yarnUrl")
    var response: String = ""
    try
    {
      response = scala.io.Source.fromURL(yarnUrl).mkString
    }
    catch
      {
        case _: SocketException =>
        SelfLogger.printLog("Catch SocketException", "WARN")
          val ssLContext: SSLContext = SSLContext.getInstance("SSL")
          ssLContext.init(null, Array(TrustAll), new java.security.SecureRandom())
          HttpsURLConnection.setDefaultSSLSocketFactory(ssLContext.getSocketFactory)
          HttpsURLConnection.setDefaultHostnameVerifier(VerifiesAllHostNames)

          val changeYarnUrl = yarnUrl.replace("http", "https")
          SelfLogger.printLog(s"Try to get resource info from $changeYarnUrl")
          response = scala.io.Source.fromURL(changeYarnUrl).mkString
        case _: SSLHandshakeException =>
        SelfLogger.printLog("Catch SSLHandshakeException", "WARN")
          val ssLContext: SSLContext = SSLContext.getInstance("SSL")
          ssLContext.init(null, Array(TrustAll), new java.security.SecureRandom())
          HttpsURLConnection.setDefaultSSLSocketFactory(ssLContext.getSocketFactory)
          HttpsURLConnection.setDefaultHostnameVerifier(VerifiesAllHostNames)

          val changeYarnUrl = yarnUrl.replace("http", "https")
          SelfLogger.printLog(s"Try to get resource info from $changeYarnUrl")
          response = scala.io.Source.fromURL(changeYarnUrl).mkString
      }
    s"Aggregate Resource Allocation: ${""""memorySeconds":[0-9]{1,}""".r.findFirstIn(response).mkString}, ${""""vcoreSeconds":[0-9]{1,}""".r.findFirstIn(response).mkString}"
  }

  private[this] def getYarnUrl(appId: String): String = sparkSession.sparkContext.getConf.getAll.filter(x => x._2.contains("http") && x._2.contains(appId)).head._2.split("/")(2)

}
