package ru.sberbank.bigdata.cloud.p4d.fns_exp.resourcemonitor.fakessl

import javax.net.ssl._

object VerifiesAllHostNames extends HostnameVerifier {
  def verify(s: String, sslSession: SSLSession) = true
 }

