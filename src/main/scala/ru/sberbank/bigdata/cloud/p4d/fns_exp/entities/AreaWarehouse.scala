package ru.sberbank.bigdata.cloud.p4d.fns_exp.entities

import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.entities.IllegalAreaNameException

object AreaWarehouse {
  private[this] val areas: Map[String, Area] = Map(
    "stg" -> new Area("stg")
    ,"aux" -> new Area("aux")
    ,"pa"  -> new Area("pa")
    ,"bkp" -> new Area("bkp")
    ,"ini" -> new Area("ini")
  )

  def getArea(areaName: String): Area = areas.getOrElse(areaName, throwIllegalAreaNameException(areaName))

  def getAreaTables(areaName: String): List[Table] = {
    val area: Area = areas.getOrElse(areaName, throwIllegalAreaNameException(areaName))
    area.getTables
  }

  private def throwIllegalAreaNameException(areaName: String): Area = {
    SelfLogger.printLog(s"Area with name $areaName does not exist", "ERROR")
    throw new IllegalAreaNameException
  }
}
