package ru.sberbank.bigdata.cloud.p4d.fns_exp.scenario

import java.util.Calendar

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, Row}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Runner, SelfLogger}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild.{BuildExecutor, EntityBuilder, TableBuilder, UnionTableBuilder, ViewFromQueryBuilder, ViewsManager}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryExecutor, QueryManager, SqlQuery}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, DateHelper, TableHelper, Tabulator}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.changes.ChangesQueryManager
import ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.increment.IncrementQueryManager
import ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.result.FormsQueryManager
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.AreaWarehouse
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import org.apache.spark.sql.{AnalysisException, SparkSession}
import org.apache.spark.sql.functions.{broadcast, count, expr, max, min, rank, row_number, sum, to_json, first}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.archive.ArchiveQueryManager

import scala.collection.mutable.ArrayBuffer
import scala.util.Try

class IncrementScenario extends Scenario {
  val executor: BuildExecutor = new BuildExecutor()
  var haveChange: Boolean = false

  private[this] val startTime: Long = Calendar.getInstance().getTimeInMillis

  override def run(): Boolean = {

    SelfLogger.printLog("Increment scenario start")

    /** Извлечение максимальных load_dt для талицы источника */
    SelfLogger.printLog("Getting maximum source dates change")
    val iniTableList = AreaWarehouse.getArea("ini").getTables
    // Только таблицы с load_dt
    val iniTableListWith = iniTableList.filter(x => x.fields.contains("ctl_validfrom"))
    SelfLogger.printLog(s"Table with ctl_validfrom: ${iniTableListWith.map(_.tableName).mkString(",")}")
    val iniMaxMap = TableHelper.getMaxDateTableMap(tbls = iniTableListWith.map(_.tableName), clmn = "ctl_validfrom", schema = Runner.conf.iniDb())

    /**
     * Извлечения  из kpd_report области PA последних
     * ctl_validfrom и src_ctlvalidfrom для каждой сущности
     */
    val spark = QueryExecutor.defaultSparkSessionGetOrCreate()
    import spark.implicits._

    SelfLogger.printLog("Getting change dates of last loading from kpd_report")
    val kpdDF = spark.table(s"${Runner.conf.paDb()}.kpd_report")
    val byKeyW = Window.partitionBy('area, 'entity).orderBy('ctl_validfrom.desc)
    val kpdLastDF = kpdDF.select('*)
      .withColumn("rn", rank over byKeyW)
      .where('rn === 1)
      .drop('rn)
    kpdLastDF.cache()

    val kpdMap = kpdLastDF.select('area, 'entity, 'ctl_validfrom, 'src_ctl_validfrom)
      .collect().map { case Row(area: String, entity: String, ctl_validfrom: String, src_ctl_validfrom: String)
    => (area, entity, ctl_validfrom, src_ctl_validfrom)
    }
      .groupBy(_._1)
      .map { case (k, v) =>
        (k, v.map(x => (x._2, (x._3, x._4))).toMap)
      }

    println("\n" + Tabulator.format(List(List("area", "entity", "ctl_validfrom", "src_ctl_validfrom"))
      ++ kpdMap.flatMap { case (k, v) => v.map { case (i, j) => Seq(k, i, j._1, j._2) } }))


    /**
     * Если дата максимальная дата из kpd_report больше или равна
     * максимальной дате источника, то холостой запуск
     */
    val maxSrcVal = iniMaxMap.values.max
    val maxPaVal = Try(kpdMap.values.flatMap(_.values.map(_._1)).max).toOption.getOrElse("")
    println(s"max of source = $maxSrcVal")
    println(s"max ctl_validfrom from kpd_report = $maxPaVal")

    if (maxPaVal >= maxSrcVal) {
      SelfLogger.printLog("No changes")
      return false
    }

    val targetNamesList: List[String] = List("egrul","egrip")

for(entName<-targetNamesList) {
  SelfLogger.printLog(s"Creating Dataframe for $entName")
  val tempViewNameMap = ArrayBuffer[(String, String)]()
  val archiveQueryManager: QueryManager = new ArchiveQueryManager
  archiveQueryManager.updateQueryData("regStr1", Runner.regStr)
  val qry = archiveQueryManager.getQueryForTable(s"${entName}_pre")
  val df = QueryExecutor.queryToDataFrame(qry)

  println("DF2")
  val df2 = df.filter(s"ctl_validfrom>${iniMaxMap.getOrElse(entName,"")}")
  tempViewNameMap += createOrReplaceTempViewFromDf(s"${entName}_pre", df2)

  println("DF3")
  val df3 = df2.select($"ogrn")

  val targetQueryManager: QueryManager = new FormsQueryManager
  targetQueryManager.initCtlData()
  targetQueryManager.updateQueryData("regStr1", Runner.regStr)
  tempViewNameMap.foreach { x => targetQueryManager.updateQueryData(x._1, x._2) }

  val qry2 = targetQueryManager.getQueryForTable(entName)

  println("DF7")
  val df7 = QueryExecutor.queryToDataFrame(qry2)

  println("DF71")

  val df71 = spark.table(s"${Runner.conf.paDb()}.$entName")

  println("DF8")
  val df8 = df71.join(broadcast(df3), df71("ogrn") === df3("ogrn"), "inner").drop(df3("ogrn"))

  println("DF9")
  val df9 = df8.withColumn("rn", row_number()
    .over(Window.partitionBy("ogrn")
      .orderBy($"ctl_validfrom".desc)))

  println("DF10")
  val df10 = df9.filter($"rn" === 1).drop($"rn")

  println("DF11")
  val df11 = getRealInc(df7, df10, entName)

  println("DF12")
  val df12 = df11.select($"validfrom_month").distinct()

  println("DF13")
  val df13 = df71.join(broadcast(df12), df71("validfrom_month") === df12("validfrom_month"), "inner").drop(df12("validfrom_month"))

  println("DF14")
  val df14 = df11.union(df13)
  createOrReplaceTempViewFromDf(entName, df14)
}

    SelfLogger.printLog(s"Calc tables...")

    val incrimentQueryManager: QueryManager = new IncrementQueryManager
    val targetBuildersList: List[EntityBuilder] = for (formName <- targetNamesList) yield {
      new TableBuilder(formName, incrimentQueryManager)
    }

    executor.execute(targetBuildersList, concurrentEnable = false)


    SelfLogger.printLog("Increment scenario complete")
    TableHelper.saveIniTablesStatuses(iniMaxMap)
    true

  }


  def getAreaValueKpd(df: DataFrame, area: String, clmn: String): Map[String, String] = {
    val ds = df.select("entity", clmn).filter(s"entity=$area").collect()
    val mp = ds.map(x => (Option(x.getString(0)).getOrElse("") -> Option(x.getString(1)).getOrElse("")))
      .toMap
    println("\n" + Tabulator.format(List(List("entity", clmn)) ++ mp.map { case (k, v) => Seq(k, v) }.toList))
    mp
  }


  /**
   * Функция формирования Dataframe со сверкой рассчитанноuj инкремента
   * с опубликованными данными
   * dfNew - Dataframe  рассчитанного инкремента
   * dfNew - Dataframe  опубликованных данных
   * tableName - наименование опубликованной таблицы
   */

  private def getRealInc(dfNew: DataFrame, dfOld: DataFrame, tableName: String): DataFrame = {

    val spark: SparkSession = QueryExecutor.defaultSparkSessionGetOrCreate()
    import spark.implicits._

    // Если отсутствует режим force-load и существуют опубликованные данные
    if (!Runner.getForceLoadFlag && TableHelper.tableHaveResultForQuery("pa", tableName)) {
      SelfLogger.printLog("Creating dataframe for comparing STG and PA", s"${tableName.toUpperCase} INFO")

      val initClmns = dfNew.columns
      //println(s"initClmns: ${initClmns.toList}")

      val compClmns = dfNew.schema.fields
        .filter(x => List("struct", "array")
          .contains(x.dataType.typeName)).map(_.name)
      //println(s"compClmns: ${compClmns.toList}")

      val dfNewj = compClmns.foldLeft(dfNew)((dfNew, x) => dfNew.withColumn(x + "_", to_json($"$x")))
      val dfOldj = compClmns.foldLeft(dfOld)((dfOld, x) => dfOld.withColumn(x + "_", to_json($"$x")))

      val techClmns = List("validfrom"
        , "load_date"
        , "filename"
        , "session_id"
        , "src_ctl_validfrom"
        , "ctl_validfrom"
        , "ctl_loading"
        , "validfrom_month") ++ compClmns
      //println(s"techClmns: $techClmns")


      val bsClmns = dfNewj.schema.fields.filter(x => !(techClmns).contains(x.name)).map(x => x.name)

      val dfOldWithSrc = dfOldj.withColumn("src_", expr("'2'"))
      val dfNewWithSrc = dfNewj.withColumn("src_", expr("'1'"))
      // .withColumn("ctl_loading",expr("0"))

      val dfUnion = dfOldWithSrc.union(dfNewWithSrc)

      val aggClnms = techClmns.map(x => first(x).as(x)) ++ Array(sum("src_").as("src_"))

      //println(s"aggClnms: ${aggClnms.toList}")

      val byPrKey = Window.partitionBy("ogrn")
      val dfGroup = dfUnion.groupBy(bsClmns.head, bsClmns.tail: _*)
        .agg(aggClnms.head, aggClnms.tail: _*)
        .withColumn("cnt", count("*") over byPrKey)

      val dfUpd = dfGroup.where($"src_" === "1")
        .withColumn("ctl_action", expr("case when cnt = 2 and src_ = 1 then 'U' else ctl_action end"))
        .select(initClmns.head, initClmns.tail: _*)
      SelfLogger.printLog("SUCCESS", s"${tableName.toUpperCase} INFO")
      dfUpd
    }
    else {
      dfNew
    }
  }

}