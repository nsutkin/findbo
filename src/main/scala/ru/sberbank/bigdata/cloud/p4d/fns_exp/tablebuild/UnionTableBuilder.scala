package ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild
import java.util.Calendar

import org.apache.spark.sql.DataFrame
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{RecoveryManager, Runner, SelfLogger}

class UnionTableBuilder(val tableName: String, val tablesParts: DataFrame*) extends EntityBuilder {
  val startTime: Long = Calendar.getInstance().getTimeInMillis

  override def build(): DataFrame = {
    SelfLogger.printLog(s"Start unite data frames in $tableName.", s"INFO $threadTeg")

    // Проверка рекавери и возврат даннх из stg
    if (Runner.getRecoveryFlag && RecoveryManager.tableIsComplete(tableName)) {
      SelfLogger.printLog(s"Table $tableName is complete in stg", s"$threadTeg RECOVERY")
      SelfLogger.printLog(s"Return table $tableName data frame from stg", s"$threadTeg RECOVERY")
      return getReadyDataFromStg
    }
    else if (Runner.getRecoveryFlag) SelfLogger.printLog(s"Table $tableName not complete in stg", s"$threadTeg RECOVERY")

    // Получаем запрос таблицы и создаем на ее основе data frame
    val tableDF: DataFrame = tablesParts.reduce(_.union(_)).cache()

    // Сохранение data frame таблицы в hdfs
    val haveNewData: Boolean = trySaveTableAndCheckNewFiles(tableDF)

    // Получаем время выполнения
    val tableEndTime: Long = Calendar.getInstance().getTimeInMillis - startTime

    SelfLogger.printLog(s"The unite data frames in $tableName is completed. " +
      s"${getChangeMessage(haveNewData)}", s"INFO $threadTeg")
    tableDF
  }
}
