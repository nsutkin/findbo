package ru.sberbank.bigdata.cloud.p4d.fns_exp.parquetwork

import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.RepartitionException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{FileHelper, StringHelper}
import org.apache.commons.lang3.exception.ExceptionUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, FileSystem, FileUtil, Path}
import org.apache.spark.sql.SparkSession
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor

import scala.concurrent._
import ExecutionContext.Implicits.global

object Repart {
  val spark: SparkSession = QueryExecutor.defaultSparkSessionGetOrCreate()
  val fs: FileSystem = FileHelper.getFileSystem
  val blockSize: Long = fs.getDefaultBlockSize
  val deltaPercent: Double = 0.1
  var i: Int = 0

  def repartTable(tableDir: String, partitionNameMask: String = ""): Int = {
    SelfLogger.printLog(s"Start running repartition for dir $tableDir")
    i = 0

    val parts: Array[FileStatus] = fs.globStatus(new Path(s"${StringHelper.removeLastSlash(tableDir)}/${if(partitionNameMask!="")s"$partitionNameMask/" else ""}"))
    val repartitionFutures: Array[Future[Int]] = parts.map(fileStatus => Future{repartDir(fileStatus)})
    val repartitionResultCodes: Array[Int] = repartitionFutures.map(Await.result(_, duration.Duration.Inf))
    if (repartitionResultCodes.contains(1)) throw new RepartitionException
    SelfLogger.printLog(s"Completed. New partition number is $i")
    i
  }

  private def getRepartCount (delta: Double, dir: Path): Int = {
    val dirSize = fs.getContentSummary(dir).getLength
    val fileCount = fs.getContentSummary(dir).getFileCount
    val newPartitionCount = ( dirSize / blockSize ) + 1
    val lowDeltaRange = newPartitionCount - newPartitionCount * deltaPercent
    val highDeltaRange = newPartitionCount + newPartitionCount * deltaPercent

    if ( lowDeltaRange <= fileCount && fileCount <= highDeltaRange || fileCount == 0 )  0
    else  newPartitionCount.toInt
  }

  private def repartDir(partitionDir: FileStatus): Int = {
    if ( fs.isDirectory(partitionDir.getPath) ) {
      val newPartitionCount = getRepartCount(deltaPercent, partitionDir.getPath)
      if ( newPartitionCount != 0) {
        val sparkPath : String = partitionDir.getPath.toString + "/*.parquet"
        val tmpSparkPath : String = partitionDir.getPath.toString + "/_tmp/"
        try {
          spark.read.parquet(sparkPath).coalesce(newPartitionCount).write.parquet(tmpSparkPath)
          val existFiles = fs.globStatus(new Path(sparkPath))
          val newFiles = fs.globStatus(new Path(tmpSparkPath  + "*.parquet"))
          existFiles.foreach(f => fs.delete(f.getPath, true))
          newFiles.foreach(f => FileUtil.copy(fs, f.getPath, fs, partitionDir.getPath, true, new Configuration()))
          fs.delete(new Path(tmpSparkPath),true)
          SelfLogger.printLog(partitionDir.getPath.getName + " was repartition")
          i += 1
          0
        }
        catch {
          case e: Exception =>
            println(ExceptionUtils.getStackTrace(e))
            1
        }
      }
      else {
        SelfLogger.printLog("No need repartition for  = " + partitionDir.getPath.getName)
        0
      }

    }
    else 0
  }

}
