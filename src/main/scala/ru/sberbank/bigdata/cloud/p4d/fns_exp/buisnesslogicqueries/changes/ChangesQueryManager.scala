package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.changes

import ru.sberbank.bigdata.cloud.p4d.fns_exp.Runner
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryManager, SqlQuery}

class ChangesQueryManager extends QueryManager {
  private[this] val iniDb: String = Runner.conf.iniDb()
  private[this] val stgDb: String = Runner.conf.stgDb()
  private[this] val paDb: String = Runner.conf.paDb()


  override def getQueryForTable(tableName: String): SqlQuery =
    tableName match {
    case _ => throw new IllegalArgumentException
  }

}
