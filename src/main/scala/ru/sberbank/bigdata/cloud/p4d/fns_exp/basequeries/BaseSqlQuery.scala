package ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries

import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.CheckException.check
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.basequery.BaseSqlQueryParamException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.AreaHelper

/*Описание.
Трэйт создан специально для запросов, которые получают информацию из kpd_report.
Так как kpd_report должен присутствовать во всех разботках витрин и реплик p4d, запросы названы "базовыми"(base)
, то есть универсальными.*/

/*Предназначение.
Проверка входящих параметров.
 */
trait BaseSqlQuery extends SqlQuery {
  check(sourceName.equals(AreaHelper.getAreaDb("stg")) || sourceName.equals(AreaHelper.getAreaDb("pa")),
    "Wrong sourceName param.",
    new BaseSqlQueryParamException)
}
