package ru.sberbank.bigdata.cloud.p4d.fns_exp.entities

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, FileHelper, TableHelper}

class Table(val area: String, val tableName: String) {
  // TODO настроить все действия с таблицами через экземпляры этого класса
  private[this] val fieldsAndPartitions: List[String] = TableHelper.findTableFields(area, tableName, withCtlFields = true)
  private[this] val tableFilesFormat: String = "parquet"

  val fields: List[String] = fieldsAndPartitions.head.split(",").toList
  val partitions: List[String] = if (fieldsAndPartitions(1).isEmpty) List() else fieldsAndPartitions(1).split(",").toList
  val directory: String = if (area!="ini") AreaHelper.getAreaDir(area).concat(s"/$tableName") else ""

  def havePartitions: Boolean = partitions.nonEmpty

  def haveFiles: Boolean = !FileHelper.directoryIsEmpty(directory)

  def repair(): Unit =
    QueryExecutor.queryToDataFrame(s"MSCK REPAIR TABLE ${AreaHelper.getAreaDb(area)}.$tableName")

  // Метод удаляет из директории таблицы все файлы и директории не являющиеся партициями или паркетниками
  def cleanGarbage: Unit = FileHelper.cleanDirectory(directory, getTableFilesSaveKey)

  def getTableFilesSaveKey: String = if (havePartitions) partitions.head else tableFilesFormat
}
