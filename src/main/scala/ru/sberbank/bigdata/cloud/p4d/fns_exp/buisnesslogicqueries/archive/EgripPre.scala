package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.archive

import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.SqlQuery

class EgripPre(val tableName: String, val sourceName: String, val queryData: Map[String, String]) extends SqlQuery{

  override def getQueryText: String = {
    raw"""select
       |trim(regexp_replace(innfl, ${queryData("regStr1")} ,' ')) as innfl
       |,trim(regexp_replace(ogrnip, ${queryData("regStr1")} ,' ')) as ogrnip
       |,trim(regexp_replace(dataogrnip, ${queryData("regStr1")} ,' ')) as dataogrnip
       |,svfl
       |,svgrazhd
       |,svadrmzh
       |,svadrelpochty
       |,svstatus
       |,svprekrash
       |,svokved
       |,trim(regexp_replace(datavyp, ${queryData("regStr1")} ,' ')) as datavyp
       |,trim(regexp_replace(file_name, ${queryData("regStr1")} ,' ')) as file_name
       |,trim(regexp_replace(ctl_validfrom, ${queryData("regStr1")} ,' ')) as ctl_validfrom
       |from $sourceName.egrip""".stripMargin
  }
}

