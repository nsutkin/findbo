package ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.result

import ru.sberbank.bigdata.cloud.p4d.fns_exp.Runner
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryManager, SqlQuery}

class FormsQueryManager extends QueryManager{

  private[this] val stgDb: String = Runner.conf.stgDb()

  override def getQueryForTable(tableName: String): SqlQuery = tableName match {
    case "egrul" => new Egrul("","",queryData)
    case "egrip" => new Egrip("","",queryData)
    case _ => throw new IllegalArgumentException
  }

}
