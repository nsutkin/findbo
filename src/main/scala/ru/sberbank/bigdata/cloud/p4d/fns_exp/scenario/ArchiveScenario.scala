package ru.sberbank.bigdata.cloud.p4d.fns_exp.scenario

import org.apache.hadoop.fs.Path
import ru.sberbank.bigdata.cloud.p4d.fns_exp.{Runner, SelfLogger}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild.{BuildExecutor, EntityBuilder, TableBuilder, ViewsManager}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.{QueryExecutor, QueryManager}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.{AreaHelper, FileHelper, TableHelper}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.archive.ArchiveQueryManager
import ru.sberbank.bigdata.cloud.p4d.fns_exp.buisnesslogicqueries.result.FormsQueryManager
import ru.sberbank.bigdata.cloud.p4d.fns_exp.entities.AreaWarehouse

import scala.collection.mutable.ArrayBuffer

class ArchiveScenario extends Scenario {
  override def run(): Boolean = {
    val executor: BuildExecutor = new BuildExecutor()

    SelfLogger.printLog("Archive scenario start")

    val iniTableList = AreaWarehouse.getArea("ini").getTables
    val iniMaxMap = TableHelper.getMaxDateTableMap(tbls = iniTableList.map(_.tableName),clmn = "ctl_validfrom",schema=Runner.conf.iniDb())

    // пересоздаем таблицы PA
    AreaHelper.recreateArea("pa")

    // рассчет таблиц
    val preStageTables: List[String] = List("egrip_pre", "egrul_pre")
    val archiveQueryManager: QueryManager = new ArchiveQueryManager
    archiveQueryManager.updateQueryData("regStr1", Runner.regStr)

    val tempViewNameMap = ArrayBuffer[(String,String)]()

    preStageTables.foreach{ x=>
      val qry = archiveQueryManager.getQueryForTable(x)
      val df = QueryExecutor.queryToDataFrame(qry)
      tempViewNameMap += createOrReplaceTempViewFromDf(x,df,cch = false)
    }

    // расчет целевых таблиц
    val targetNamesList: List[String] = List("egrip","egrul")
    val targetQueryManager: QueryManager = new FormsQueryManager
    targetQueryManager.initCtlData()
    targetQueryManager.updateQueryData("regStr1", Runner.regStr)
    tempViewNameMap.foreach {x => targetQueryManager.updateQueryData(x._1, x._2)}

    val targetBuildersList: List[EntityBuilder] = for (formName <- targetNamesList) yield {
      var cch = false
      new TableBuilder(formName, targetQueryManager,cch)
    }

    executor.execute(targetBuildersList, concurrentEnable = false)

    val sizePerRec: Map[String,Double] = Map("egrul" -> 2.84, "egrip" -> 1.44)
    val fileSize = 650
    val spark = QueryExecutor.defaultSparkSessionGetOrCreate()

    import java.text.SimpleDateFormat
    val fileDt = new SimpleDateFormat("yyyyMMdd_HHmmss").format(
          new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(Runner.conf.ctlValidfrom()))

//    targetNamesList.foreach{x=>
//      FileHelper.exportData (new Path(AreaHelper.getAreaDir("stg")+"/export")
//      ,x
//      ,spark.table(s"${AreaHelper.getAreaDb("stg")}.$x")
//      ,FileHelper.hdfs
//      ,List("src_ctl_validfrom","ctl_action","ctl_validfrom","ctl_loading")
//      ,sizePerRec(x)
//      ,fileSize
//      ,fileDt
//      ,Runner.conf.ctlLoading())
//    }

    SelfLogger.printLog("Archive scenario complete")
    TableHelper.saveIniTablesStatuses(iniMaxMap)
    true
  }


}
