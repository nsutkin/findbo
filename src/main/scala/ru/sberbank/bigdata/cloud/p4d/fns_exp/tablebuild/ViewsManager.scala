package ru.sberbank.bigdata.cloud.p4d.fns_exp.tablebuild

import org.apache.spark.sql.{DataFrame, SparkSession}
import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger.printLog
import ru.sberbank.bigdata.cloud.p4d.fns_exp.basequeries.QueryExecutor

object ViewsManager {
  private[this] var existingViewsNames: Array[String] = Array()
  val session: SparkSession = QueryExecutor.defaultSparkSessionGetOrCreate()

  // Удаление всех вьюх, для освобождения памяти
  def deleteTempViews(): Unit = {
    printLog("Start drop temp views")
    printLog(s"Existing temp views: ${existingViewsNames.mkString(",")}")
    for (tempView <- existingViewsNames)
      if(session.catalog.dropTempView(tempView)) printLog(s"Drop view $tempView SUCCESS")
      else printLog(s"Drop view $tempView FAIL", "WARN")
  }

  // Метод создан для логирования и учета созданных views.
  // Учет необходим для дальнейшего удаления.
  def createOrReplaceTempViewFromDF(viewName: String, source: DataFrame): Unit = {
    source.createOrReplaceTempView(viewName)
    printLog(s"Temp view $viewName is created")
    existingViewsNames = Array.concat(existingViewsNames, Array(viewName))
  }

  def existingViewsCount: Int = existingViewsNames.length

}
