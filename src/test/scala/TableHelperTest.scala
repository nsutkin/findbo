/*
import org.scalatest.FlatSpec
import org.apache.hadoop.fs.Path
import ru.sberbank.bigdata.cloud.p4d.fns_exp.SelfLogger
import ru.sberbank.bigdata.cloud.p4d.fns_exp.exceptions.TableNotFoundInDDLException
import ru.sberbank.bigdata.cloud.p4d.fns_exp.helpers.TableHelper

class TableHelperTest extends FlatSpec  {

//  "TableHelpers method findTableFields " should "return PA forms_list names string" in {
//      // res.head - поля, res(1) - партиции
//      val res: List[String] = TableHelper.findTableFields("pa", "forms_list",
//        new Path("./devops/ci/vvod/dwh/hive").toString)
//      assert(res.head.equals("form_code,form_id,form_name,form_version,report_id,report_dt,report_version,ready_to_use," +
//        "report_descr,report_signed_dt,report_signed_by,source_id,source_name")
//        && res(1).equals("report_dt_month"))
//    }
//
//  "TableHelpers method findTableFields " should "return PA f_lizing names string" in {
//    // res.head - поля, res(1) - партиции
//    val res: List[String] = TableHelper.findTableFields("pa", "f_lizing",
//      new Path("./devops/ci/vvod/dwh/hive").toString)
//    assert(res.head.equals("form_code,form_id,form_version,report_id,report_dt,report_version,ready_to_use,source_id," +
//      "source_name,sbl_manager_fio,sbl_filial,sb_cred_tb_name,sb_cred_catb,sb_cred_gosb,sb_cred_osb,sb_client_tb_name," +
//      "sb_client_region,sb_client_tb_head,sb_client_gosb,sb_client_osb,cl_inn_kio,cl_name,cl_kpp,cl_ogrn,cl_okved_code," +
//      "cl_okved_name,liz_agr_num,liz_agr_specification,liz_agr_type,liz_rate,liz_open_dt,cred_agr_num,cred_rate_actual," +
//      "cred_agr_open_dt,cred_agr_close_dt,cred_agr_limit,cred_agr_currency,cred_issue_currency,sum_issued_quarter," +
//      "sum_returned_quarter,sum_debt_actual,sum_debt_sdo_month,sum_debt_sdo_quarter,sum_overdue_actual," +
//      "sum_overdue_sdo_quarter,sum_npl90_actual,sum_net_income_month,sum_prcnt_income_month,sum_net_income_sbl_month," +
//      "sells_status,comment")
//      && res(1).equals("report_dt_month"))
//  }
//
//  "TableHelpers method findTableFields " should "return AUX forms fields string" in {
//      val res: List[String] = TableHelper.findTableFields("aux", "forms",
//        new Path("./devops/ci/vvod/dwh/hive").toString)
//      assert(res.head.equals("id,ext_id,name")
//        && res(1).equals(""))
//    }
//
//  "TableHelpers method findTableFields " should "return AUX forms full fields string" in {
//    val res: List[String] = TableHelper.findTableFields("aux", "forms",
//      new Path("./devops/ci/vvod/dwh/hive").toString, withCtlFields = true)
//    assert(res.head.equals("id,ext_id,name,ctl_loading,ctl_action,ctl_validfrom")
//      && res(1).equals(""))
//  }
//
//  "TableHelpers method findTableFields " should "return AUX form_cells full fields string" in {
//    val res: List[String] = TableHelper.findTableFields("aux", "form_cells",
//      new Path("./devops/ci/vvod/dwh/hive").toString, withCtlFields = true)
//    SelfLogger.printLog(s"fields form_cells: ${res.head}")
//    assert(res.head.equals("form_id,vernum,ext_id,dict_id,ctl_loading,ctl_action,ctl_validfrom")
//      && res(1).equals(""))
//  }
//
//  "TableHelpers method findTableFields " should "return AUX dictionary_values full fields string" in {
//    val res: List[String] = TableHelper.findTableFields("aux", "dictionary_values",
//      new Path("./devops/ci/vvod/dwh/hive").toString, withCtlFields = true)
//    SelfLogger.printLog(s"fields dictionary_values: ${res.head}")
//    assert(res.head.equals("dict_id,val_id,code,value_name,attr0,ctl_loading,ctl_action,ctl_validfrom")
//      && res(1).equals(""))
//  }
//
//  "TableHelpers method getTableArea " should "return 'aux' for aux table report_data string" in {
//      assert(TableHelper.getTableArea("report_data", new Path("./devops/ci/vvod/dwh/hive").toString).equals("aux"))
//    }
//
//  "TableHelpers method getTableArea " should "return 'aux' for aux table forms string" in {
//    assert(TableHelper.getTableArea("forms", new Path("./devops/ci/vvod/dwh/hive").toString).equals("aux"))
//  }
//
//  "TableHelpers method getTableArea " should "return 'pa' for pa table forms_list string" in {
//    assert(TableHelper.getTableArea("forms_list", new Path("./devops/ci/vvod/dwh/hive").toString).equals("pa"))
//  }
//
//  it should "produce TableNotFoundInDDLException when call method getTableArea with not exist table name" in {
//      assertThrows[TableNotFoundInDDLException] {
//          TableHelper.getTableArea("SomeNotExistTableName", new Path("./devops/ci/vvod/dwh/hive").toString)
//        }
//    }
//
//  "TableHelpers method findTablePartitions " should "return AUX forms empty partitions names list" in {
//    val res: List[String] = TableHelper.findTablePartitions("aux", "forms",
//      new Path("./devops/ci/vvod/dwh/hive").toString)
//    assert(res.head.isEmpty)
//  }
//
//  "TableHelpers method findTablePartitions " should "return AUX report_data partitions names list" in {
//    val res: List[String] = TableHelper.findTablePartitions("aux", "report_data",
//      new Path("./devops/ci/vvod/dwh/hive").toString)
//    assert(res.contains("report_dt_month"))
//  }
//
//  "TableHelpers method findTablePartitions " should "return PA forms_list partitions names list" in {
//    val res: List[String] = TableHelper.findTablePartitions("pa", "forms_list",
//      new Path("./devops/ci/vvod/dwh/hive").toString)
//    assert(res.head.nonEmpty, res.contains("report_dt_month"))
//  }

}
*/
