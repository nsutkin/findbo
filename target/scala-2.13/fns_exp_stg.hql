DROP TABLE IF EXISTS information_schema.egrip;
CREATE EXTERNAL TABLE information_schema.egrip(
  inn string
  ,ogrn string
  ,ogrn_date string
  ,gender string
  ,fullname  string
  ,citizenship_type string
  ,citizenship_name string
  ,country string
  ,address struct<indeks:string, kodregion: string, kodadrkladr: string, dom: string, korpus: string, kvart: string, region:string, raion: string, gorod:string, naselpunkt: string, ulica:string>
  ,email string
  ,state_code string
  ,state_name string
  ,term_state_code string
  ,term_state_name string
  ,term_state_date string
  ,okved array<struct<kodokved:string,naimokved:string,main:string>>
  ,validfrom string
  ,load_date string
  ,filename string
  ,session_id string
  ,src_ctl_validfrom string
  ,ctl_action string
  ,ctl_validfrom string
  ,ctl_loading bigint)
PARTITIONED BY (
    validfrom_month string
)
STORED AS PARQUET
location '/root/custom/cib/p4d/fns_exp/stg/egrip';
MSCK REPAIR TABLE information_schema.egrip;


DROP TABLE IF EXISTS information_schema.egrul;
CREATE EXTERNAL TABLE information_schema.egrul(
  inn string
  ,kpp string
  ,fullname string
  ,name string
  ,email string
  ,address struct<indeks:string, kodregion: string, kodadrkladr: string, dom: string, korpus: string, kvart: string, region:string, raion: string, gorod:string, naselpunkt: string, ulica:string>
  ,ogrn string
  ,ogrn_date string
  ,kodopf string
  ,naimopf string
  ,spropf string
  ,isfilial string
  ,okved array<struct<kodokved:string,naimokved:string,main:string>>
  ,state array<struct<state_code:string,state_name:string,state_except_date:string,state_except_num:string,state_except_pub_date:string,state_except_mag_num:string>>
  ,date_of_termination string
  ,code_of_termination string
  ,name_of_termination string
  ,validfrom string
  ,load_date string
  ,filename string
  ,session_id string
  ,src_ctl_validfrom string
  ,ctl_action string
  ,ctl_validfrom string
  ,ctl_loading bigint)
PARTITIONED BY (
    validfrom_month string
)
STORED AS PARQUET
location '/root/custom/cib/p4d/fns_exp/stg/egrul';
MSCK REPAIR TABLE information_schema.egrul;


DROP TABLE IF EXISTS information_schema.kpd_report;
CREATE EXTERNAL TABLE IF NOT EXISTS information_schema.kpd_report (
     entity                 string          COMMENT 'Шаг расчета (сущность)'
    ,area                   string          COMMENT 'Область таблицы'
    ,entity_finished_dttm   string          COMMENT 'Дата и время окончания расчета сущности'
    ,entity_duration_sec    bigint          COMMENT 'Длительность расчета сущности'
    ,ctl_validfrom          string          COMMENT 'Дата и время старта загрузки CTL'
    ,src_ctl_validfrom      string          COMMENT 'Дата и время актуальности данных источника'
    ,isForceLoad            string          COMMENT 'Параметр запуска потока true/false'
    ,part_list              array<string>   COMMENT 'Список обновленных партиций'
)
PARTITIONED BY (
     ctl_loading             bigint          COMMENT 'Идентификатор загрузки CTL, в рамках которой завершился расчет шага'
)
STORED AS PARQUET
location '/root/custom/cib/p4d/findbo/pa/kpd_report';
MSCK REPAIR TABLE information_schema.kpd_report;
 