DROP TABLE IF EXISTS information_schema.dbo_monthly_report_bkp;
CREATE EXTERNAL TABLE information_schema.dbo_monthly_report_bkp(
accdebnumber		string		 COMMENT 'Номер ЛОРО счета плательщика'
,datasource			string 		 COMMENT 'Источник данных (ЕКС/Афина)'
,code_channel		string		 COMMENT 'Канал дистрибуци (SWIFT / MQFI / UFEBS)'
,dformat			string		 COMMENT 'Формат документа'
,num_payments		bigint		 COMMENT 'Кол-во платежей'
,ctl_loading		bigint	 	 COMMENT 'Идентификатор загрузки CTL'
,ctl_validfrom		string	 	 COMMENT 'Время загрузки CTL'
,ctl_action			string 		 COMMENT 'Операция над записью'
)
PARTITIONED BY (
    report_period	string		 COMMENT 'ПАРТИЦИЯ - Отчетный период в формате ГГГГ-ММ'
)
STORED AS PARQUET
location '/root/custom/cib/p4d/findbo/bkp/dbo_monthly_report_bkp';
MSCK REPAIR TABLE information_schema.dbo_monthly_report;



