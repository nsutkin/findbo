DROP VIEW IF EXISTS new_z_ac_fin;
CREATE VIEW IF NOT EXISTS new_z_ac_fin
AS
select
  cast(c_main_v_id  as string) 			as c_main_v_id
  ,cast(id			as decimal(38,12))  as id
from information_schema.z_ac_fin;

DROP VIEW IF EXISTS new_z_cl_part;
CREATE VIEW IF NOT EXISTS new_z_cl_part
AS
select
  cast(c_code 		as string) 			as c_code
  ,cast(id			as decimal(38,12))  as id
from information_schema.z_cl_part;

DROP VIEW IF EXISTS viev_z_docum_rc;
CREATE VIEW IF NOT EXISTS viev_z_docum_rc
AS
select
  cast(c_crclcode				as string) 			as c_crclcode
  ,cast(c_crdate 				as string) 			as c_crdate
  ,cast(c_dog_rc				as decimal(38,12))  as c_dog_rc
  ,cast(c_first_ref				as decimal(38,12))  as c_first_ref
  ,cast(c_iso					as string) 			as c_iso
  ,cast(c_next_ref				as decimal(38,12))  as c_next_ref
  ,cast(c_number_doc 			as string) 			as c_number_doc
  ,cast(c_original_rcclcode 	as string) 			as c_original_crclcode
  ,cast(C_ORIGINAL_NUMBER_DOC	as string) 			as c_original_number_doc
  ,cast(C_ORIGINAL_TYPE_DOC 	as string) 			as c_original_type_doc
  ,cast(C_PAYER_ACC 			as string) 			as c_payer_acc
  ,cast(c_payer_bacc 			as string) 			as c_payer_bacc
  ,cast(c_receiver_acc  		as string) 			as c_receiver_acc
  ,cast(c_receiver_bacc 		as string) 			as c_receiver_bacc
  ,cast(c_sub_type 				as string) 			as c_sub_type
  ,cast(c_summa 				as decimal(17,2))   as c_summa
  ,cast(c_type_doc 				as string) 			as c_type_doc
  ,cast(c_valdate 				as string) 			as c_valdate
  ,cast(id 						as decimal(38,12))  as id
from information_schema.z_docum_rc;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.z_trc;
CREATE VIEW IF NOT EXISTS z_trc
AS
select
 cast(c_client	as string) 			as c_client
from information_schema.z_trc;

drop table if exists information_schema.bell_dbo_eks_detailed;
CREATE table IF NOT EXISTS information_schema.bell_dbo_eks_detailed
AS
SELECT *
FROM (
------>>Данные без комиссий
SELECT
ADED.GREGOR_DT
,ADED.DR_ID
,ADED.DATE_RC
,ADED.C_VALDATE
,ADED.CODE_CHANNEL
,ADED.C_ORIGINAL_CRCLCODE
,ADED.C_CRCLCODE
,ADED.PAYER_ACC_LORO
,ADED.C_PAYER_ACC
,ADED.C_RECEIVER_ACC
,ADED.C_PAYER_BACC
,ADED.C_RECEIVER_BACC
,ADED.C_TYPE_DOC
,ADED.C_ORIGINAL_TYPE_DOC
,ADED.C_SUB_TYPE
,ADED.C_ORIGINAL_SUBTYPE_DOC
,ADED.C_NUMBER_DOC
,ADED.C_ORIGINAL_NUMBER_DOC
,ADED.C_FIRST_REF
,ADED.C_NEXT_REF
,ADED.C_SUMMA
,ADED.C_ISO
,ADED.C_CLIENT_DT
,ADED.C_CLIENT_CR
,ADED.PAYER_CODE
,ADED.RECEIVER_CODE

,DR.STATE_ID
,DR.C_MAIN_DOC

,ACC_DT.C_MAIN_V_ID C_ACC_DT
,ACC_KT.C_MAIN_V_ID C_ACC_KT

    ,Decode(ZR.C_DT, '1','Дебет','Кредит') C_DT
    ,ZR.C_START_SUM, ZR.C_SUMMA ZR_C_SUMMA
    ,ZR.C_START_SUM+ Decode(ZR.C_DT, '1',-ZR.C_SUMMA, ZR.C_SUMMA) C_END_SUM
    ,ZR.C_START_SUM_NAT, ZR.C_SUMMA_NAT
    ,ZR.C_START_SUM_NAT+ Decode(ZR.C_DT, '1',-ZR.C_SUMMA_NAT, ZR.C_SUMMA_NAT) C_END_SUM_NAT
    ,ZR.C_DATE

            ,sum(ZR.c_summa_nat) over (partition by ADED.c_crclcode order by ZR.C_DATE) as sum
            ,count(ZR.c_summa_nat) over (partition by ADED.c_crclcode, ZR.C_DATE) as count
	    ,percentile_approx(ZR.c_summa_nat, 0.5) over (partition by ADED.c_crclcode, ZR.c_date) as median

,AF.C_MAIN_V_ID ZR_C_ACC_CORR
,'without' clmn1
FROM
information_schema.A_DBO_EKS_DETAILED ADED
INNER JOIN
information_schema.Z_DOCUM_RC DR
ON ADED.DR_ID = DR.ID --OR ADED.C_FIRST_REF = DR.ID
AND DR.STATE_ID IN ('S_12','S_14','S_16') --Документы РЦ со статусами 12, 14, 16

INNER JOIN information_schema.Z_MAIN_DOCUM MD
ON DR.C_MAIN_DOC = MD.ID
AND MD.STATE_ID IN ( 'PROV' , 'PAY' ) --Платёжные документы со статусами 'Проведён' , 'Оплачен'

LEFT JOIN information_schema.Z_AC_FIN ACC_DT ON MD.C_ACC_DT = ACC_DT.ID --Счёт ДТ в плат. документах
LEFT JOIN information_schema.Z_AC_FIN ACC_KT ON MD.C_ACC_KT = ACC_KT.ID --Счёт КТ в плат. документах
INNER JOIN information_schema.Z_RECORDS ZR ON MD.ID = ZR.C_DOC --Проводки(транзакции)

JOIN information_schema.Z_AC_FIN AF
ON ZR.C_ACC_CORR = AF.ID --Корр.счёт для проводки

UNION ALL ------>>Комиссии

SELECT
 ADED.GREGOR_DT
,ADED.DR_ID
,ADED.DATE_RC
,ADED.C_VALDATE
,ADED.CODE_CHANNEL
,ADED.C_ORIGINAL_CRCLCODE
,ADED.C_CRCLCODE
,ADED.PAYER_ACC_LORO
,ADED.C_PAYER_ACC
,ADED.C_RECEIVER_ACC
,ADED.C_PAYER_BACC
,ADED.C_RECEIVER_BACC
,ADED.C_TYPE_DOC
,ADED.C_ORIGINAL_TYPE_DOC
,ADED.C_SUB_TYPE
,ADED.C_ORIGINAL_SUBTYPE_DOC
,ADED.C_NUMBER_DOC
,ADED.C_ORIGINAL_NUMBER_DOC
,ADED.C_FIRST_REF
,ADED.C_NEXT_REF
,ADED.C_SUMMA
,ADED.C_ISO
,ADED.C_CLIENT_DT
,ADED.C_CLIENT_CR
,ADED.PAYER_CODE
,ADED.RECEIVER_CODE

,DR.STATE_ID
,DR.C_MAIN_DOC

,ACC_DT.C_MAIN_V_ID C_ACC_DT
,ACC_KT.C_MAIN_V_ID C_ACC_KT

    ,Decode(ZR.C_DT, '1','Дебет','Кредит') C_DT
    ,ZR.C_START_SUM, ZR.C_SUMMA ZR_C_SUMMA
    ,ZR.C_START_SUM+ Decode(ZR.C_DT, '1',-ZR.C_SUMMA, ZR.C_SUMMA) C_END_SUM
    ,ZR.C_START_SUM_NAT, ZR.C_SUMMA_NAT
    ,ZR.C_START_SUM_NAT+ Decode(ZR.C_DT, '1',-ZR.C_SUMMA_NAT, ZR.C_SUMMA_NAT) C_END_SUM_NAT
    ,ZR.C_DATE

            ,sum(ZR.c_summa_nat) over (partition by ADED.c_crclcode order by ZR.C_DATE) as sum
            ,count(ZR.c_summa_nat) over (partition by ADED.c_crclcode, ZR.C_DATE) as count
            ,percentile_approx(ZR.c_summa_nat, 0.5) over (partition by ADED.c_crclcode, ZR.c_date) as median

,AF.C_MAIN_V_ID ZR_C_ACC_CORR
,'with' clmn1
FROM
information_schema.A_DBO_EKS_DETAILED ADED
INNER JOIN information_schema.Z_DOCUM_RC DR
ON ADED.DR_ID = DR.ID
--AND DR.STATE_ID IN ('S_12','S_14','S_16') --Документы РЦ со статусами 12, 14, 16

INNER JOIN information_schema.Z_MAIN_DOCUM MD
ON DR.C_MAIN_DOC = MD.ID
AND MD.STATE_ID IN ( 'PROV' , 'PAY' ) --Платёжные документы со статусами 'Проведён' , 'Оплачен'

LEFT JOIN information_schema.Z_AC_FIN ACC_DT ON MD.C_ACC_DT = ACC_DT.ID --Счёт ДТ в плат. документах
LEFT JOIN information_schema.Z_AC_FIN ACC_KT ON MD.C_ACC_KT = ACC_KT.ID --Счёт КТ в плат. документах
INNER JOIN information_schema.Z_RECORDS ZR ON MD.ID = ZR.C_DOC --Проводки(транзакции)

JOIN information_schema.Z_AC_FIN AF
ON ZR.C_ACC_CORR = AF.ID --Корр.счёт для проводки

WHERE 1=1
AND SUBSTR(ACC_KT.C_MAIN_V_ID,1,3)||'-'||SUBSTR(ACC_KT.C_MAIN_V_ID,14,7)
IN
('706-2710100','706-2720101','706-2720102','706-2720104','706-2720105','706-2720107','706-2740101','706-2740199','706-2740500','706-2790101')
) ORDER BY 1,C_MAIN_DOC;

