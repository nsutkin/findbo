name := "findbo"
organization := "ru.sberbank.bigdata.cloud.p4d.findbo"

val distVersion = Option(System.getProperty("version")).getOrElse("1.0.0")
version := distVersion

scalaVersion := "2.13.8"

val sparkVersion = "3.2.1"
val scallopVersion = "4.0.2"
val pureconfigVersion = "0.8.0"
val scalatestVersion = "3.2.10"
val scalacticVersion = "3.0.4"
val commonsLogging = "1.1.3"


libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-hive" % sparkVersion,
  "org.rogach" %% "scallop" % scallopVersion,
  "org.scalatest" %% "scalatest" % scalatestVersion % Test
  //"org.scalactic" %%  "scalactic" % scalacticVersion
)

enablePlugins(PackPlugin)

packResourceDir += (baseDirectory.value / s"target/scala-2.13/findbo-assembly-${version.value}.jar" -> s"hdfs/__OOZIEAPPDIR__/dwh/jar/findbo_2.11.jar")
packResourceDir += (baseDirectory.value / "devops/ci/findbo/dwh/ctl/entities/" -> s"ctl/entities/create/")
packResourceDir += (baseDirectory.value / "devops/ci/findbo/dwh/ctl/workflows/" -> s"ctl/workflows/create/")
packResourceDir += (baseDirectory.value / "devops/ci/findbo/dwh/hive"  -> s"hive")
packResourceDir += (baseDirectory.value / "devops" / "ci" / "findbo" / "dwh" / "hive_to_hdfs"  -> s"hdfs/__OOZIEAPPDIR__/dwh/hive")
packResourceDir += (baseDirectory.value / "devops" / "ci" / "findbo" / "dwh" / "oozie"  -> s"hdfs/__OOZIEAPPDIR__/dwh/oozie")
packResourceDir += (baseDirectory.value / "devops" / "ci" / "findbo" / "dwh" / "ref"  -> s"hdfs/__OOZIEAPPDIR__/dwh/ref")
packResourceDir += (baseDirectory.value / "devops" / "ci" / "findbo" / "dwh" /  "__VERSION__.txt"  -> s"hdfs/__OOZIEAPPDIR__/dwh/__VERSION__.txt")

packArchive <<= packArchive dependsOn (assembly)