#!/usr/bin/env bash

. functions.sh

args=()

while [ $# -gt 0 ] ; do
    case $1 in
        --ctl)
            shift
            ctl=$1
            ;;
        --hiveJdbcUrl)
            shift
            hiveJdbcUrl=$1
            ;;
        --hueUrl)
            shift
            hueUrl=$1
            ;;
        --jobTracker)
            shift
            jobTracker=$1
            ;;
        --nameNode)
            shift
            nameNode=$1
            ;;
        --oozie)
            shift
            oozie=$1
            ;;
        --loading_id)
            shift
            loading_id=$1
            ;;
        --wf_name)
            shift
            wf_name=$1
            ;;
        --user.name)
            shift
            user_name=$1
            ;;
        --wf_id)
            shift
            wf_id=$1
            ;;
        --oozie_path)
            shift
            oozie_path=$1
            ;;
        --ctl_entity_id)
            shift
            ctl_entity_id=$1
            ;;
        --kerb_principal)
            shift
            kerb_principal=$1
            ;;
        --kerb_keytab)
            shift
            kerb_keytab=$1
            ;;
        --kerb_domain)
            shift
            kerb_domain=$1
            ;;
        --etl_ini_db)
            shift
            etl_ini_db=$1
            ;;
        --etl_stg_db)
            shift
            etl_stg_db=$1
            ;;
        --etl_bkp_db)
            shift
            etl_bkp_db=$1
            ;;
        --etl_pa_db)
            shift
            etl_pa_db=$1
            ;;
        --etl_ini_dir)
            shift
            etl_ini_dir=$1
            ;;
       --etl_stg_dir)
            shift
            etl_stg_dir=$1
            ;;
      --etl_pa_dir)
            shift
             etl_pa_dir=$1
            ;;
       --etl_bkp_dir)
            shift
            etl_bkp_dir=$1
            ;;
       --etl_recovery_mode)
            shift
            etl_recovery_mode=$1
            ;;
        --etl_cleanup_stg)
            shift
            etl_cleanup_stg=$1
            ;;
        --etl_force_load)
            shift
            etl_force_load=$1
            ;;
        --jar_name)
            shift
            jar_name=$1
            ;;
        --jar_class)
            shift
            jar_class=$1
            ;;
        --y_driver_mem)
            shift
            y_driver_mem=$1
            ;;
        --y_executor_mem)
            shift
            y_executor_mem=$1
            ;;
        --y_min_executors)
            shift
            y_min_executors=$1
            ;;
        --y_max_executors)
            shift
            y_max_executors=$1
            ;;
        --y_executor_cores)
            shift
            y_executor_cores=$1
            ;;
        --y_queue)
            shift
            y_queue=$1
            ;;
        --y_try_init_num)
            shift
            y_try_init_num=$1
            ;;
        --y_try_init_delay_m)
            shift
            y_try_init_delay_m=$1
            ;;
        --parallel_calc)
            shift
            parallel_calc=$1
            ;;
        *)
 #           args[$#args[@]}]=$1
 #           ;;
    esac
    shift
done

# Variables
y_try_init_delay_m=$y_try_init_delay_m'm'
v_ctl_validfrom=$(date +'%Y-%m-%d %H:%M:%S')
v_wf_path="p4d/$wf_name/"
v_aux_path=$(pwd)'/'
app_id="client4d-data-custom-findbo"
v_log_path="/data/applogs/"$app_id"/"
v_log="$v_log_path"$(date --date="$v_ctl_validfrom" +'%Y_%m_%d__%H_%M_%S')"__$loading_id.javatime.log"
v_hdfs_aux_path="$nameNode$oozie_path$v_wf_path"
v_hdfs_logs_path="$nameNode$oozie_path$v_wf_path"logs
#v_aux_log_path=$infaAuxLogsPath"client4d/"$wf_name"/"
#v_aux_log="$v_aux_log_path"$(date --date="$v_ctl_validfrom" +'%Y_%m_%d__%H_%M_%S')"__$loading_id.log"
v_keytab=$(basename $kerb_keytab)

# Create log file
if [[ -n $wf_name ]] ; then
    :
else
    message="ERROR: parameter wf_name is empty, aborting workflow"
    echo $message
    f_wf_abort $message
fi

rm -rf "$v_log_path"
mkdir -p "$v_log_path"

if touch "$v_log"
then
    chmod -R 777 $(dirname "$v_log_path")
    chmod -R 777 "$v_log_path"
else
    message="ERROR: can not create log file $v_log, aborting workflow"
    echo $message
    f_wf_abort $message
fi

# Start log file
exec 6>&1 7>&2 1>>$v_log 2>>$v_log

echo VERSON 1.0.1

set +x ; f_log SUCCESS: Log started.
echo Oozie workflow xml path in hdfs = $v_hdfs_aux_path'dwh/oozie/profile.xml'
echo v_log = $v_log
echo current user = $(whoami)
echo hostname = $(hostname)
echo klist:
klist | grep -v '^$'

# Отправка информации в CTL - текущий хост и имя файла лога
set +x ; f_log INFO: Sending status to CTL: hostname and logs file location
message="Oozie main shell task started successfully. Host: $(hostname). Session directory: $v_aux_path. Log: $v_log."
f_status_ctl RUNNING "$message"

# Параметры и переменные
set +x ; f_log INFO: Workflow parameters and variables:
echo CTL GLOBAL PARAMETERS

f_chk_param ctl $ctl
f_chk_param hiveJdbcUrl $hiveJdbcUrl
f_chk_param hiveMetastoreUri $hiveMetastoreUri
f_chk_param hueUrl $hueUrl
f_chk_param jobTracker $jobTracker
f_chk_param oozie $oozie
f_chk_param nameNode $nameNode

echo
echo CTL IMPLICIT PARAMETERS

f_chk_param wf_id $wf_id
f_chk_param loading_id $loading_id

echo
echo CTL WORKFLOW LOCAL PARAMETERS

f_chk_param wf_name $wf_name
f_chk_param oozie_path $oozie_path
f_chk_param ctl_entity_id $ctl_entity_id
f_chk_param kerb_principal $kerb_principal
f_chk_param kerb_keytab $kerb_keytab
f_chk_param kerb_domain $kerb_domain
f_chk_param etl_ini_dir $etl_ini_dir
f_chk_param etl_ini_db $etl_ini_db
f_chk_param etl_stg_dir $etl_stg_dir
f_chk_param etl_stg_db $etl_stg_db
f_chk_param etl_bkp_dir $etl_bkp_dir
f_chk_param etl_bkp_db $etl_bkp_db
f_chk_param etl_aux_dir $etl_aux_dir
f_chk_param etl_aux_db $etl_aux_db
f_chk_param etl_pa_dir $etl_pa_dir
f_chk_param etl_pa_db $etl_pa_db
f_chk_param etl_recovery_mode $etl_recovery_mode
f_chk_param etl_cleanup_stg $etl_cleanup_stg
f_chk_param etl_force_load $etl_force_load
f_chk_param kerb_domain $kerb_domain
f_chk_param jar_name $jar_name
f_chk_param jar_class $jar_class
f_chk_param y_driver_mem $y_driver_mem
f_chk_param y_executor_mem $y_executor_mem
f_chk_param y_min_executors $y_min_executors
f_chk_param y_max_executors $y_max_executors
f_chk_param y_executor_cores $y_executor_cores
f_chk_param y_try_init_delay_m $y_try_init_delay_m
f_chk_param y_try_init_num $y_try_init_num
f_chk_param y_queue $y_queue
f_chk_param y_kryobuf_m $y_kryobuf_m
f_chk_param dynamic_allocation $dynamic_allocation


echo
echo VARIABLES

f_chk_param v_log $v_log
f_chk_param v_ctl_validfrom $v_ctl_validfrom
f_chk_param v_log_path $v_log_path
f_chk_param v_hdfs_logs_path $v_hdfs_logs_path
f_chk_param v_hdfs_aux_path $v_hdfs_aux_path
f_chk_param v_keytab $v_keytab

echo
echo Environment variables old values:
echo HADOOP_CONF_DIR =  $HADOOP_CONF_DIR

echo
echo Environment variables new values:
export HADOOP_CONF_DIR=/etc/hive/conf
echo HADOOP_CONF_DIR = $HADOOP_CONF_DIR

set +x ; f_log INFO Checking 1: kerb_keytab - keytab for ETL user ; set -x
if [[ $(whoami) == $kerb_principal ]] ; then

    if kinit -Vkt "$v_keytab" "$kerb_principal@$kerb_domain" ; then
        echo SUCCESS: KERB_KEYTAB
    else
        message="ERROR: KERB_KEYTAB"
        echo $message
        f_wf_abort $message
    fi
else
    if kinit -Vkt "$v_keytab" -c "$v_aux_path$kerb_principal".tkt "$kerb_principal@$kerb_domain" ; then
        echo KRB5CCNAME = "$KRB5CCNAME"
        export KRB5CCNAME="FILE:$v_aux_path$kerb_principal".tkt
        klist
        echo SUCCESS: KERB_KEYTAB
    else
        message="ERROR: KERB_KEYTAB"
        echo $message
        f_wf_abort $message
    fi
fi

set +x ; f_log INFO Checking 2: HDFSAuxLogs parameters:
set -x
if hadoop fs -test -e $v_hdfs_logs_path; then
    echo SUCCESS: HDFSAuxLogs exists
elif
    command=$(hadoop fs -mkdir -p $v_hdfs_logs_path 2>&1)
    hadoop fs -chmod -R 775 $v_hdfs_logs_path
    then
         echo SUCCESS: HDFSAuxLogs created
 else
    set +x
    message="ERROR: HDFS logs   $command"
    echo $message
    f_wf_abort "$message"
 fi

set +x ; f_log INFO Checking 3: dwh files in AuxPath:
set -x
#cd "$v_aux_path"
echo Loading from HDFS:
if hadoop fs -get "$v_hdfs_aux_path"dwh ; then
    chmod -Rv 777 "dwh/"
    echo SUCCESS: dwh files dowloaded
else
    message="ERROR: can not load files from HDFS: $v_hdfs_aux_path"dwh
    echo $message
    f_wf_abort "$message"
fi


set +x ; f_log INFO Stage 5: Clear workflow local tmp dir
set -x
#cd "$v_aux_path"
rm -rfv "tmp"
mkdir "tmp"
chmod -R 777 "tmp"

set +x ; f_log INFO Stage 6: Clearing HDFS STG dir
set -x
if [[ 0 == $etl_recovery_mode ]] ; then
    hadoop fs -rm -R $nameNode$etl_stg_dir*
    echo SUCCESS: Clearing HDFS STG
else
    echo INFO: etl_recovery_mode = $etl_recovery_mode, clearing HDFS STG skipped
fi

# Запуск spark-submit ####################################################
set +x ; f_log Running spark-submit
appName="L=$loading_id WF=$wf_id NM=$wf_name CT=cb/p4d/custom"
set -x

count=1
[ "$y_try_init_num" -lt 1 ] && y_try_init_num=1

while [[ "$v_spark_submit_exit_code" != 0 ]] ; do

    set +x ; f_log "INFO: SPARK INITIALIZATION. TRY: $count in $y_try_init_num"
    set -x
    f_status_ctl RUNNING "SPARK INITIALIZATION. TRY: $count in $y_try_init_num"


    # Задержка при повторных запусках
    if [ "$count" -gt 1 ] ; then
        sleep $y_try_init_delay_m
    fi

    # Выполнение spark-submit
    f_run_spark
    v_spark_submit_exit_code=$?
    set +x ; f_log INFO: spark-submit exit code = "$v_spark_submit_exit_code"

    set -x
    f_status_ctl RUNNING "Spark-submit exit code = $v_spark_submit_exit_code. "$spark_command

    # Проверка exit code
    if [[ "$v_spark_submit_exit_code" == 0 ]] ; then
        set +x ; f_log "SUCCESS: spark-submit finished"
        set -x
    elif [[ "$v_spark_submit_exit_code" == 102 ]] && [[ "$count" -le "$y_try_init_num" ]] ; then
        set +x ; f_log "INFO: spark context initialization error, retry..."
        set -x
        ((count ++))
    else
        set +x ; f_log "ERROR: spark-submit failed"
        set -x
        f_wf_abort "ERROR: spark-submit failed"
    fi
done

# Публикация статистик в CTL
set +x ; f_log "INFO Stage 7: YARN application id"
set -x
val=$(cat tmp/application_id.txt)
if [[ -n "$val" ]] ; then
    echo  SUCCESS: application id = $val
    curl -d '{"loading_id": '$loading_id', "entity_id": '$ctl_entity_id', "stat_id": 15, "avalue": ["'$val'"]}' -H "Content-Type: application/json" -X POST $ctl'/v1/api/statval/m'
else
    set +x ; f_log "ERROR: no application id"
fi

set +x ; f_log "INFO Stage 8: MAX_CDC_DATE = src_ctl_validfrom"
set -x
src_max_dt=$(sort tmp/src_max_dt_* | tail -n 1)
if [[ -n "$src_max_dt" ]] ; then
    echo  SUCCESS: new MAX_CDC_DATE = $src_max_dt ;
    curl -d '{"loading_id": '$loading_id', "entity_id": '$ctl_entity_id', "stat_id": 1, "avalue": ["'"$src_max_dt"'"]}' -H "Content-Type: application/json" -X POST $ctl'/v1/api/statval/m'
else
    src_max_dt="Old"
    set +x ; f_log "WARN: new MAX_CDC_DATE value not found, setting MAX_CDC_DATE = Old"
fi

set +x ; f_log "INFO Stage 9: CHANGE"
set -x
change=$(sort tmp/change* | tail -n 1)
if [[ "$change" = 1 ]] ; then
    echo SUCCESS: change = $change
    curl -d '{"loading_id": '$loading_id', "entity_id": '$ctl_entity_id', "stat_id": 2, "avalue": ["'1'"]}' -H "Content-Type: application/json" -X POST $ctl'/v1/api/statval/m'
else
    set +x ; f_log "WARN: NO CHANGE"
fi

set +x ; f_log "INFO Stage 10: new BUSINESS_DATE"
set -x
bus_date=$(sort tmp/bus_max_dt_* | tail -n 1)
if [[ -n "$bus_date" ]] ; then
    echo  SUCCESS: new BUSINESS_DATE = $bus_date
    curl -d '{"loading_id": '$loading_id', "entity_id": '$ctl_entity_id', "stat_id": 5, "avalue": ["'"$bus_date"'"]}' -H "Content-Type: application/json" -X POST $ctl'/v1/api/statval/m'
else
    bus_date="Old"
    set +x ; f_log "WARN: new BUSINESS_DATE value not found"
fi

set +x ; f_log "INFO Stage 11: new LAST_LOADED_TIME"
curl -d '{"loading_id": '$loading_id', "entity_id": '$ctl_entity_id', "stat_id": 11, "avalue": ["'"$v_ctl_validfrom"'"]}' -H "Content-Type: application/json" -X POST $ctl'/v1/api/statval/m'


set +x ; f_log "INFO Stage 12: log statistics to CTL"
message="SUCCESS. YARN Application id: $val. CHANGE = $change. New MAX_CDC_DATE /src_ctl_validfrom/ = $src_max_dt. New BUSINESS_DATE = $bus_date. Logs at host: $(hostname): $v_log. Logs at HDFS: $v_hdfs_logs_path."

f_status_ctl RUNNING "$message"

set +x ; f_log Publishing logs
f_zip_logs