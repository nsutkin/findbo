function f_log {
    echo
    echo $(date +"%Y-%m-%d %H:%M:%S,%3N") $@
}

function f_run_spark {
set -x
spark_command=$((spark2-submit \
    --master yarn  --queue "$y_queue"\
    --executor-memory "$y_executor_mem" \
    --driver-memory "$y_driver_mem" \
    --executor-cores "$y_executor_cores" \
    --conf spark.dynamicallocation.minexecutors=$y_min_executors \
    --conf spark.dynamicallocation.maxexecutors=$y_max_executors \
    --conf spark.dynamicAllocation.enabled=$dynamic_allocation \
    --principal "$kerb_principal" \
    --keytab "$v_keytab" \
    --class "$jar_class"\
    --conf spark.ui.showConsoleProgress=false \
    --conf spark.kryoserializer.buffer.max="$y_kryobuf" \
    --conf spark.executor.memoryOverhead=2048 \
    --conf spark.driver.memoryOverhead=2048 \
    "dwh/jar/$jar_name" \
    --app-name "$appName" \
    --etl-recovery-mode "$etl_recovery_mode" \
    --ctl-loading "$loading_id"  \
    --etl-cleanup-stg "$etl_cleanup_stg" \
    --etl-force-load "$etl_force_load" \
    --ini-db "$etl_ini_db" \
    --stg-dir "$etl_stg_dir" \
	  --stg-db "$etl_stg_db" \
    --bkp-dir "$etl_bkp_dir" \
    --bkp-db "$etl_bkp_db" \
    --aux-dir "$etl_aux_dir" \
    --aux-db "$etl_aux_db" \
    --pa-dir "$etl_pa_dir" \
    --pa-db "$etl_pa_db" \


    ) 2>&1 >> $v_log)
}



function f_zip_logs {

        cd $v_log_path
        set +x ; f_log Try to get hdfs archive logs.; set -x
        hadoop fs -get $v_hdfs_logs_path"/$(date +'%Y_%m')"* $v_log_path
        ls -al

        set +x ; f_log Try to untar archive logs.; set -x
        tar -xzf *.gz

        set +x ; f_log Try to rm hdfs archive logs.; set -x
        rm $v_log_path*.gz
        ls -al
        set +x ; f_log Try to tar archive logs.; set -x
        tar -zcvf $v_log_path"/$(date +'%Y_%m')".tar.gz *.log

        set +x ; f_log INFO loading logs to HDFS; set -x
        hadoop fs -put -f $v_log_path*.gz  $v_hdfs_logs_path
}

function f_status_ctl {
    set +x
    current_date=$(date +'%Y-%m-%d %H:%M:%S')
    current_status=$1
    shift
    current_messages="$@"
    messages=${current_messages//'"'/}
    echo message:     $messages
    set -x
    curl -d  '{"loading_id": '$loading_id', "effective_from": "'"$current_date"'", "status": "'"$current_status"'", "log": "'"$messages"'"}' -H "Content-Type: application/json" -X PUT $ctl'/v1/api/loading/status'
    set +x
}

function f_wf_abort {
    set -x
    f_status_ctl ERROR $@
    f_zip_logs
    set +x ; f_log INFO aborting CTL workflow
    set -x
    curl -X DELETE $ctl'/v1/api/loading/'$loading_id
    exit 1
}

function f_data_validation {
    n=0
		m=0
		for file in $(hadoop fs -ls -C "$inbox""$data_mask"*.zip) ; do
			shortname=$(basename $file)
			shortname=$(echo $shortname | tr [:lower:] [:upper:])
			n=$[$n+1];
			array_inbox[$n]=$shortname
		done

		for file1 in $(hadoop fs -ls -C "$etl_src_dir""$data_mask"*.zip) ; do
			shortname1=$(basename $file1)
			shortname1=$(echo $shortname1 | tr [:lower:] [:upper:])
			m=$[$m+1];
			array_etl_src_dir[$m]=$shortname1
		done
}

function f_data_array_after_inbox {
        m=0
				n=0
				for file in $(hadoop fs -ls -C "$inbox""$data_mask"*.zip) ; do
					shortname_after=$(basename $file)
					echo $shortname_after
					m=$[$m+1];
					array_after_inbox[$m]=$shortname_after
				done
}

function f_data_array_before_etl_src_dir {
d=0
for file1 in $(hadoop fs -ls -C "$etl_src_dir""$data_mask"*.zip) ; do
			shortname_before=$(basename $file1)
			echo $shortname_before
			d=$[$d+1];
			array_before_etl_src_dir[$d]=$shortname_before
		done
}


function f_chk_param {
   if [ -z "$1" ]                           # Длина аргумента #1 равна нулю?
   then
     echo "The argument has zero length"  # Или аргумент не был передан функции.
	 message=" WARN: The argument has zero length"
	 f_status_ctl RUNNING  $message
   fi


   if [ -z "$2" ]
   then
	 find_null_param=1
	 echo " $1 = $2 "
     message=" WARN: Option $1 is not specified"
	 f_status_ctl RUNNING  $message
	else
	 echo " $1 = $2 "
   fi

   return 0
}