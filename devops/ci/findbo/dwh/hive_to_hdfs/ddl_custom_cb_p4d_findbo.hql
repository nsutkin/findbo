DROP TABLE IF EXISTS custom_cib_p4d_findbo.dbo_monthly_report;
CREATE EXTERNAL TABLE custom_cib_p4d_findbo.dbo_monthly_report(
	accdebnumber	string	COMMENT 'Номер ЛОРО счета плательщика'
	,datasource		string	COMMENT 'Источник данных (ЕКС/Афина)'
	,code_channel	string	COMMENT 'Канал дистрибуци (SWIFT / MQFI / UFEBS)'
	,dformat		string	COMMENT 'Формат документа'
	,num_payments	bigint	COMMENT 'Кол-во платежей'
	,ctl_loading	bigint	COMMENT 'Идентификатор загрузки CTL'
	,ctl_validfrom	string	COMMENT 'Время загрузки CTL'
	,ctl_action		string	COMMENT 'Операция над записью'
)
PARTITIONED BY (
    format_doc		string	COMMENT 'Формат документа'
)
STORED AS PARQUET
location '/data/custom/cib/p4d/findbo/pa/dbo_monthly_report';
MSCK REPAIR TABLE custom_cib_p4d_findbo.dbo_monthly_report;


DROP TABLE IF EXISTS custom_cib_p4d_findbo.kpd_report;
CREATE EXTERNAL TABLE IF NOT EXISTS custom_cib_p4d_findbo.kpd_report (
     entity                 string          COMMENT 'Шаг расчета (сущность)'
    ,area                   string          COMMENT 'Область таблицы'
    ,entity_finished_dttm   string          COMMENT 'Дата и время окончания расчета сущности'
    ,entity_duration_sec    bigint          COMMENT 'Длительность расчета сущности'
    ,ctl_validfrom          string          COMMENT 'Дата и время старта загрузки CTL'
    ,src_ctl_validfrom      string          COMMENT 'Дата и время актуальности данных источника'
    ,isForceLoad            string          COMMENT 'Параметр запуска потока true/false'
    ,part_list              array<string>   COMMENT 'Список обновленных партиций'
)
PARTITIONED BY (
     ctl_loading             bigint          COMMENT 'Идентификатор загрузки CTL, в рамках которой завершился расчет шага'
)
STORED AS PARQUET
location '/data/custom/cib/p4d/findbo/pa/kpd_report';
MSCK REPAIR TABLE custom_cib_p4d_findbo.kpd_report;

