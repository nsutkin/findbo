DROP TABLE IF EXISTS custom_cib_p4d_findbo_bkp.dbo_monthly_report_bkp;
CREATE EXTERNAL TABLE custom_cib_p4d_findbo_bkp.dbo_monthly_report_bkp(
report_period		string		 COMMENT 'ПАРТИЦИЯ - Отчетный период в формате ГГГГ-ММ'
,accdebnumber		string		 COMMENT 'Номер ЛОРО счета плательщика'
,datasource			string 		 COMMENT 'Источник данных (ЕКС/Афина)'
,code_channel		string		 COMMENT 'Канал дистрибуци (SWIFT / MQFI / UFEBS)'
,dformat			string		 COMMENT 'Формат документа'
,num_payments		bigint		 COMMENT 'Кол-во платежей'
,ctl_loading		bigint	 	 COMMENT 'Идентификатор загрузки CTL'
,ctl_validfrom		string	 	 COMMENT 'Время загрузки CTL'
,ctl_action			string 		 COMMENT 'Операция над записью'
)
PARTITIONED BY (
    report_period	string		 COMMENT 'ПАРТИЦИЯ - Отчетный период в формате ГГГГ-ММ'
)
STORED AS PARQUET
location '/data/custom/cib/p4d/findbo/bkp/dbo_monthly_report_bkp';
MSCK REPAIR TABLE custom_cib_p4d_fns_exp_bkp.dbo_monthly_report_bkp;


