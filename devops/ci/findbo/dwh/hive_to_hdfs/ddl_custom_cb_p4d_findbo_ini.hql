DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.z_ac_fin;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.z_ac_fin
AS
select
  cast(c_main_v_id  as string) 			as c_main_v_id
  ,cast(id			as decimal(38,12))  as id
from internal_eks_ibs.z_ac_fin;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.z_cl_part;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.z_cl_part
AS
select
  cast(c_code 		as string) 			as c_code
  ,cast(id			as decimal(38,12))  as id
from external_fns.z_cl_part;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.z_docum_rc;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.z_docum_rc
AS
select
  cast(c_crclcode				as string) 			as c_crclcode
  ,cast(c_crdate 				as string) 			as c_crdate
  ,cast(c_dog_rc				as decimal(38,12))  as c_dog_rc
  ,cast(c_first_ref				as decimal(38,12))  as c_first_ref
  ,cast(c_iso					as string) 			as c_iso
  ,cast(c_next_ref				as decimal(38,12))  as c_next_ref
  ,cast(c_number_doc 			as string) 			as c_number_doc
  ,cast(c_original_crclcode 	as string) 			as c_original_crclcode
  ,cast(c_original_number_doc	as string) 			as c_original_number_doc
  ,cast(c_original_type_doc 	as string) 			as c_original_type_doc
  ,cast(c_payer_acc 			as string) 			as c_payer_acc
  ,cast(c_payer_bacc 			as string) 			as c_payer_bacc
  ,cast(c_receiver_acc 			as string) 			as c_receiver_acc
  ,cast(c_receiver_bacc 		as string) 			as c_receiver_bacc
  ,cast(c_sub_type 				as string) 			as c_sub_type
  ,cast(c_summa 				as decimal(17,2))   as c_summa
  ,cast(c_type_doc 				as string) 			as c_type_doc
  ,cast(c_valdate 				as string) 			as c_valdate
  ,cast(id 						as decimal(38,12))  as id
from external_fns.z_docum_rc;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.z_trc;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.z_trc
AS
select
 cast(c_client	as string) 			as c_client
from external_fns.z_trc;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.account;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.account
AS
select
  cast(code		as string) as code
  ,cast(label	as string) as label
from internal_athena_cur_od.account;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.bankoper;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.bankoper
AS
select
  cast(account			as decimal(10,0)) as account
  ,cast(doc				as decimal(10,0)) as doc
  ,cast(sumaccount		as decima(24,3))  as sumaccount
  ,cast(sumconv			as decima(24,3))  as sumconv
from internal_athena_cur_od.bankoper;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.clientattr;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.clientattr
AS
select
  cast(bik 				as string) as bik
  ,cast(clientname 		as string) as clientname 
  ,cast(country 		as string) as country
  ,cast(distch 			as string) as distch
  ,cast(inn 			as string) as inn
  ,cast(is_resident 	as int)    as is_resident
  ,cast(kpp 			as string) as kpp
  ,cast(ogrn 			as string) as ogrn
  ,cast(swift 			as string) as swift
  ,cast(tercode 		as string) as tercode
from internal_athena_cur_od.clientattr;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.customertransfer;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.customertransfer
AS
select
  cast(beneficiaryname 	as string) 		  as beneficiaryname
  ,cast(doc 			as decimal(10,0)) as doc
  ,cast(nostro			as decimal(10,0)) as nostro
  ,cast(payname			as string)		  as payname
from internal_athena_cur_od.customertransfer;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.docaccountsb;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.docaccountsb
AS
select
  cast(beneficiaryaccount 	as string) 		  as beneficiaryaccount
  ,cast(beneficiaryname 	as string) 		  as beneficiaryname
  ,cast(classified 			as decimal(10,0)) as classified
  ,cast(doclabel 			as string) 		  as doclabel
  ,cast(label 				as string) 		  as label
  ,cast(operdate 			as string) 		  as operdate
  ,cast(payaccount 			as string) 		  as payaccount
  ,cast(payname 			as string) 		  as payname
  ,cast(sumaccount 			as decimal(24,3)) as sumaccount
  ,cast(sumconv 			as decimal(24,3)) as sumconv
from internal_athena_cur_od.docaccountsb;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.doctree;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.doctree
AS
select
  cast(category 			as decimal(4,0)) as category
  ,cast(classified 			as decimal(10,0)) as classified
  ,cast(ctl_action 			as string) 		  as ctl_action
  ,cast(ctl_loading 		as bigint) 		  as ctl_loading
  ,cast(ctlvalidfrom  		as string) 		  as ctlvalidfrom
  ,cast(docstate 			as decimal(10,0)) as docstate
  ,cast(label 				as string) 		  as label
  ,cast(operdate 			as string) 		  as operdate
from internal_athena_cur_od.doctree;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.MemoOrder;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.MemoOrder
AS
select
 cast(accountcred 			as decimal(10,0)) as accountcred
 ,cast(accountdeb 			as decimal(10,0)) as accountdeb
 ,cast(doc 					as decimal(10,0)) as doc
from internal_athena_cur_od.MemoOrder;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.MetalDelivery;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.MetalDelivery
AS
select
  cast(account 				as decimal(10,0)) as account
  ,cast(corraccount 		as decimal(10,0)) as corraccount
  ,cast(doc 				as decimal(10,0)) as doc
from internal_athena_cur_od.MetalDelivery;

DROP VIEW IF EXISTS custom_cib_p4d_findbo_ini.preparemoney;
CREATE VIEW IF NOT EXISTS custom_cib_p4d_findbo_ini.preparemoney
AS
select
  cast(account				as decimal(10,0)) as account
  ,cast(doc					as decimal(10,0)) as doc
from internal_athena_cur_od.preparemoney;
